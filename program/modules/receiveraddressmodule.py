#!/usr/bin/env python
"""
Contains the main functions for introducing and retrieving the message from the
receiver address
"""

import copy
import time
import random
from getpass import getpass
from ..connection import conaux
from ..auxiliar import messagefunctions
from ..auxiliar import getcheck
from ..auxiliar import maindialoges
from ..auxiliar import cipherfunctions

__author__ = "Mar Gimenez Aguilar"
__version__ = "1"
__maintainer__ = "Mar Gimenez Aguilar"
__email__ = "mar.gimeneza@gmail.com"

def extractReceiverAddressMethod(tx_hashes,connection,ChaCha20key, password, default,ntimes, timeV ):
    if connection == "w3":
        from ..connection import w3mode
        w3,w3option = conaux.connectWeb3()
    else:
        from ..connection import rpcmode
        rpc, w3 = conaux.connectRPC()
        c = 0
    if timeV:
        medium_process_perf = 0
        time_block = 0
        times_process = []
        times_block = []
        original_hashes = copy.deepcopy(tx_hashes)
    for i in range(ntimes):
        if timeV:
            start_time_perf = time.perf_counter()
            tx_hashes = original_hashes[i]
            #print(tx_hashes)
        data_in_transactions = []
        #Extract destinations from transactions
        nonces = []
        cipherfunctions.nonce = []
        senderAddress = ""
        #print(cipherfunctions.nonce)
        #We need to check if last hash or only one transaction in the message
        last_block = 0
        for x in tx_hashes:
            if connection == "w3":
                result = w3mode.w3getTransaction([x],w3)
                data_in_transactions.append(result[0]['to'][2:])
                #print(result[0]['nonce'])
                cipherfunctions.nonce.append(result[0]['nonce'])
                senderAddress = result[0]['from']
                #print(cipherfunctions.nonce)
                if len(tx_hashes)== 1:
                    last_block = result[0]['blockNumber']
            else:
                result = rpcmode.rpcgetTransaction([x], rpc, c)
                data_in_transactions.append(result[0]['result']['to'][2:])
                senderAddress = result[0]['result']['from']
                cipherfunctions.nonce.append(int(result[0]['result']['nonce'][2:],16))
                if len(tx_hashes)== 1:
                    last_block = int(result[0]['result']['blockNumber'][2:],16)
                c += 1
        #Check if it is only one or we have to get the rest from the blockchain
        if len(tx_hashes)== 1:
            if timeV:
                time_block_ = time.perf_counter() - start_time_perf
                time_block += time_block_
                time_block_execution = time_block_
                start_time_perf_retrieving = time.perf_counter()
            nonces = copy.deepcopy(cipherfunctions.nonce)
            if password != "":
                data_in_transactions = messagefunctions.ordermessage(data_in_transactions, password)
            if ChaCha20key == "":
                ChaCha20key = cipherfunctions.generateChaCha20key(cipherfunctions.strechKey(senderAddress.lower(),nonce=""))
            nonces = copy.deepcopy(cipherfunctions.nonce)
            ChaCha20keycopy = copy.deepcopy(ChaCha20key)
            cipherfunctions.nonce = copy.deepcopy(nonces)
            nonce_first = copy.deepcopy(nonces)[0]
            #print("First nonce", nonce_first)
            #single, data_in_transactions, total,index,withmessage = messagefunctions.checkifsingle(messages, ChaCha20key)
            #print("Chacha",ChaCha20keycopy)
            len_of_length, total_length, message, in_next_transaction,first_low_length = messagefunctions.checkifsinglefullfield (data_in_transactions,1,ChaCha20keycopy)
            #print("Chacha",ChaCha20keycopy )
            #print("extracted", len_of_length, total_length, message, in_next_transaction)
            if not in_next_transaction and len(message)>= (total_length * 8):
                single = True
                print("Single!!", total_length, message)
                message = message[:(total_length * 8)]
            else:
                single = False
            #single, data_in_transactions, total,index,withmessage = messagefunctions.checkifsingle(data_in_transactions, ChaCha20key)
            # while not withmessage:
            #     while last_block > 0 and 0 nonce:
            #         if connection == "w3":
            #             data_to_retrieve, nonces_tx = w3mode.w3getBlocktransactionsbyNumber (last_block, 'from', senderAddress, 'to', w3)
            #If part of a larger message
            if timeV:
                time_extraction_perf = time.perf_counter() - start_time_perf_retrieving
                time_process_execution = time_extraction_perf
                medium_process_perf += time_extraction_perf
            if not single:
                cumulative_nonces= []
                cumulative_nonces.append(nonces[0])
                transactions_found = 1
                block_in_use = copy.deepcopy(last_block)
                all_extracted = False
                while not all_extracted:
                    if timeV:
                        start_time_perf_block = time.perf_counter()
                    if connection == "w3":
                        data_to_retrieve, nonces_tx = w3mode.w3getBlocktransactionsbyNumber (block_in_use, ['from'], ['== ' + str(senderAddress)], 'to', w3)
                    else:
                        data_to_retrieve, nonces_tx = rpcmode.rpcgetBlocktransactionsbyNumber (hex(block_in_use), ['from'], ['== ' + str(senderAddress)], 'to', rpc,c)
                        c += 1
                    if timeV:
                        time_block_ = time.perf_counter() - start_time_perf_block
                        time_block += time_block_
                        time_block_execution += time_block_
                        start_time_perf_retrieving = time.perf_counter()
                    #print(nonces_tx, data_to_retrieve)
                    if nonce_first in nonces_tx:
                        index_nonce = nonces_tx.index(nonce_first)
                        #del nonces_tx[index_nonce]
                        del data_to_retrieve[index_nonce]
                    #print(nonces_tx, data_to_retrieve)
                    if len(data_to_retrieve) > 0:
                        print("in next", in_next_transaction)
                        #print("Chacha",ChaCha20key )
                        cipherfunctions.nonce = copy.deepcopy([nonce_first])
                        message,len_of_length, total_length, message, in_next_transaction,all_extracted,first_low_length= messagefunctions.checkextractfullfilled (data_to_retrieve,1,len_of_length, total_length, message, in_next_transaction,first_low_length, ChaCha20keycopy)
                    block_in_use += 1
                    transactions_found += len(data_to_retrieve)
                    print("Transactions recovered: ", transactions_found)
                    print("Block: ", block_in_use)
                    if timeV:
                        time_extraction_perf = time.perf_counter() - start_time_perf_retrieving
                        medium_process_perf += time_extraction_perf
                        #print("añado")
                        time_process_execution += time_extraction_perf
                nonces = copy.deepcopy(cumulative_nonces)
            if timeV:
                #print("LLego")
                times_process.append(time_process_execution)
                times_block.append(time_block_execution)
                # cumulative_nonces = [-1] * total
                # cumulative_nonces[(index-1)] = nonces[0]
                # transactions_found = 0
                # block_in_use = copy.deepcopy(last_block)
                # while transactions_found < total:
                #     #Check if we have to retrieve in newer blocks, oldest or both
                #     #If the it the last transaction, we look for transactions in the current block
                #     # and older blocks
                #     if index == total:
                #         if timeV:
                #             start_time_perf_block = time.perf_counter()
                #         if connection == "w3":
                #             data_to_retrieve, nonces_tx = w3mode.w3getBlocktransactionsbyNumber (block_in_use, ['from'], ['== ' + str(senderAddress)], 'to', w3)
                #         else:
                #             data_to_retrieve, nonces_tx = rpcmode.rpcgetBlocktransactionsbyNumber (hex(block_in_use), ['from'], ['== ' + str(senderAddress)], 'to', rpc,c)
                #             c += 1
                #         if timeV:
                #             time_block_ = time.perf_counter() - start_time_perf_block
                #             time_block += time_block_
                #             time_block_execution += time_block_
                #             start_time_perf_retrieving = time.perf_counter()
                #         cipherfunctions.nonce = copy.deepcopy(nonces_tx)
                #         if password != "":
                #             data_to_retrieve = messagefunctions.ordermessage(data_to_retrieve, password)
                #         data_in_transactions ,cumulative_nonces, added = messagefunctions.checkifpartofmessage (data_to_retrieve, nonces_tx, data_in_transactions, cumulative_nonces, ChaCha20key)
                #         block_in_use -= 1
                #         transactions_found += added
                #         if timeV:
                #             time_extraction_perf = time.perf_counter() - start_time_perf_retrieving
                #             medium_process_perf += time_extraction_perf
                #             time_process_execution += time_extraction_perf
                #     elif index == 1:
                #         if timeV:
                #             start_time_perf_block = time.perf_counter()
                #         if connection == "w3":
                #             data_to_retrieve, nonces_tx = w3mode.w3getBlocktransactionsbyNumber (block_in_use, ['from'], ['== ' + str(senderAddress)], 'to', w3)
                #         else:
                #             data_to_retrieve, nonces_tx = rpcmode.rpcgetBlocktransactionsbyNumber (hex(block_in_use), ['from'], ['== ' + str(senderAddress)], 'to', rpc,c)
                #             c += 1
                #         if timeV:
                #             time_block_ = time.perf_counter() - start_time_perf_block
                #             time_block += time_block_
                #             time_block_execution += time_block_
                #             start_time_perf_retrieving = time.perf_counter()
                #         cipherfunctions.nonce = copy.deepcopy(nonces_tx)
                #         if password != "":
                #             data_to_retrieve = messagefunctions.ordermessage(data_to_retrieve, password)
                #         data_in_transactions ,cumulative_nonces, added = messagefunctions.checkifpartofmessage (data_to_retrieve, nonces_tx, data_in_transactions, cumulative_nonces, ChaCha20key)
                #         block_in_use += 1
                #         transactions_found += added
                #         if timeV:
                #             time_extraction_perf = time.perf_counter() - start_time_perf_retrieving
                #             medium_process_perf += time_extraction_perf
                #             time_process_execution += time_extraction_perf
                #     else:
                #         while cumulative_nonces[0] == -1:
                #             #Look for the transactions in older blocks including
                #             #the current one
                #             if timeV:
                #                 start_time_perf_block = time.perf_counter()
                #             if connection == "w3":
                #                 data_to_retrieve, nonces_tx = w3mode.w3getBlocktransactionsbyNumber (block_in_use, ['from'], ['== ' + str(senderAddress)], 'to', w3)
                #             else:
                #                 data_to_retrieve, nonces_tx = rpcmode.rpcgetBlocktransactionsbyNumber (hex(block_in_use), ['from'], ['== ' + str(senderAddress)], 'to', rpc,c)
                #                 c += 1
                #             if timeV:
                #                 time_block_ = time.perf_counter() - start_time_perf_block
                #                 time_block += time_block_
                #                 time_block_execution += time_block_
                #                 start_time_perf_retrieving = time.perf_counter()
                #             cipherfunctions.nonce = copy.deepcopy(nonces_tx)
                #             if password != "":
                #                 data_to_retrieve = messagefunctions.ordermessage(data_to_retrieve, password)
                #             data_in_transactions ,cumulative_nonces, added = messagefunctions.checkifpartofmessage (data_to_retrieve, nonces_tx, data_in_transactions, cumulative_nonces, ChaCha20key)
                #             block_in_use -= 1
                #             transactions_found += added
                #             if timeV:
                #                 time_extraction_perf = time.perf_counter() - start_time_perf_retrieving
                #                 medium_process_perf += time_extraction_perf
                #                 time_process_execution += time_extraction_perf
                #         block_in_use = copy.deepcopy(last_block) + 1
                #         while cumulative_nonces[-1] == -1:
                #             #Look for the transactions in newer blocks excluding
                #             # the block of the original transaction
                #             if timeV:
                #                 start_time_perf_block = time.perf_counter()
                #             if connection == "w3":
                #                 data_to_retrieve, nonces_tx = w3mode.w3getBlocktransactionsbyNumber (block_in_use, ['from'], ['== ' + str(senderAddress)], 'to', w3)
                #             else:
                #                 data_to_retrieve, nonces_tx = rpcmode.rpcgetBlocktransactionsbyNumber (hex(block_in_use), ['from'], ['== ' + str(senderAddress)], 'to', rpc,c)
                #                 c += 1
                #             if timeV:
                #                 time_block_ = time.perf_counter() - start_time_perf_block
                #                 time_block += time_block_
                #                 time_block_execution += time_block_
                #                 start_time_perf_retrieving = time.perf_counter()
                #             cipherfunctions.nonce = copy.deepcopy(nonces_tx)
                #             if password != "":
                #                 data_to_retrieve = messagefunctions.ordermessage(data_to_retrieve, password)
                #             data_in_transactions ,cumulative_nonces, added = messagefunctions.checkifpartofmessage (data_to_retrieve, nonces_tx, data_in_transactions, cumulative_nonces, ChaCha20key)
                #             block_in_use += 1
                #             transactions_found += added
                #             if timeV:
                #                 time_extraction_perf = time.perf_counter() - start_time_perf_retrieving
                #                 medium_process_perf += time_extraction_perf
                #                 time_process_execution += time_extraction_perf
                # nonces = copy.deepcopy(cumulative_nonces)
                # if timeV:
                #     times_process.append(time_process_execution)
                #     times_block.append(time_block_execution)
        #print("Data in transactions",data_in_transactions)
        if len(tx_hashes) > 1:
            print("Introduce the first transaction hash\n")
            exit(1)
            # if timeV:
            #     time_block_ = time.perf_counter() - start_time_perf
            #     time_block += time_block_
            #     time_block_execution = time_block_
            #     start_time_perf = time.perf_counter()
            # nonces = copy.deepcopy(cipherfunctions.nonce)
            # if password != "":
            #     data_in_transactions = messagefunctions.ordermessage(data_in_transactions, password)
            # #Extract the message per se
            # #first we order the blocks
            # if ChaCha20key == "":
            #     ChaCha20key = cipherfunctions.generateChaCha20key(cipherfunctions.strechKey(senderAddress.lower(),nonce=""))
            # data_in_transactions, nonces = messagefunctions.orderblocks(data_in_transactions, ChaCha20key, nonces)
            # #Then we extract the message
            # if timeV:
            #     #times_process.append(time_process_execution)
            #     times_block.append(time_block_execution)
            #     times_process.append(0)
        if timeV and len(tx_hashes) == 1:
            start_time_perf = time.perf_counter()
        #cipherfunctions.nonce = copy.deepcopy(nonces)
        #message = messagefunctions.extractmessagehex(data_in_transactions, 2, "extractReceiverAddressMethod",ChaCha20key)
        if timeV:
            time_extraction_perf = time.perf_counter() - start_time_perf
            medium_process_perf += time_extraction_perf
            times_process[-1] += time_extraction_perf
    #print("Message", message)
    message = bytearray([int(message[i:i+8], 2) for i in range(0, len(message), 8)]).hex()
    print("Message", message)
    if timeV:
        time_extraction_perf = medium_process_perf/ntimes
        time_block = time_block/ntimes
        print("Retrieving from the blockchain time (average): {} seconds".format(time_block))
        print("Retrieving from the blockchain time per execution: {} seconds".format(times_block))
        print("Revealing time (average): {} seconds".format(time_extraction_perf))
        print("Revealing time per execution: {} seconds ".format(times_process))
    return message

def ReceiverAddressMethod(data,connection, ChaCha20key, password, default,ntimes, timeV):
    #print(connection)
    if connection == "w3":
        from ..connection import w3mode
        w3,w3option = conaux.connectWeb3()
    else:
        from ..connection import rpcmode
        rpc, w3 = conaux.connectRPC()
        c = 0
    #We have to get the variable id web3
    print("Introduce the sender address\n")
    senderAddress = input()
    if ChaCha20key == "":
        ChaCha20key = cipherfunctions.generateChaCha20key(cipherfunctions.strechKey(senderAddress.lower(),nonce=""))
    key = ""
    if (connection == "w3" and w3option == "infura") or (connection != "w3" and type(w3) is not str):
        if type(w3) is not str:
            from ..connection import w3mode
            rpc = w3
            w3option = "infura"
        print("Introduce the sender private key\n")
        key = getpass()
    elif connection == "w3" and w3option != "HTTP":
        print("Introduce the sender password\n")
        key = getpass()
    if timeV:
        medium_process_perf = 0
        medium_mining_perf = 0
        medium_sending= 0
        medium_gas_time = 0
        medium_gas= 0
        gas_list = []
        times_sending = []
        times_process = []
        times_mining = []
        tx_hashes_list = []
    for i in range(ntimes):
        if timeV:
            start_time_perf = time.perf_counter()
        if connection == "w3" or type(rpc) is not str:
            cipherfunctions.nonce = w3mode.w3getAccountNonce(senderAddress,w3)
            nonce = w3mode.w3getAccountNonce(senderAddress,w3)
        else:
            cipherfunctions.nonce = int(rpcmode.rpcgetAccountNonce(senderAddress,rpc,c),16)
            c += 1
        #message_prepared = messagefunctions.preparemessagehex(data, 40, "ReceiverAddressMethod", ChaCha20key)
        message_prepared = messagefunctions.preparemessagefullfield(data, 20, ChaCha20key)
        if password != "":
            message_prepared = messagefunctions.disordermessage(message_prepared, password)
            #print(message_prepared)
        # estimatedGas = getEstimatedGasCost(message_prepared, connection, sendTransaction)
        if timeV:
            time_preparation_perf = time.perf_counter() - start_time_perf
            medium_process_perf += time_preparation_perf
            times_process.append(time_preparation_perf)
            start_time_perf = time.perf_counter()
        estimatedGas = []
        #print(connection)
        print("Memessage_prepared", message_prepared)
        for x in message_prepared:
            if connection == "w3" or type(rpc) is not str:
                estimatedGas.append(w3mode.w3estimateGasSendTransaction(senderAddress, key,('0x'+ x), w3, option=w3option))
            else:
                estimatedGas.append(rpcmode.rpcestimateGasSendTransaction(senderAddress,('0x'+ x) ,rpc,c) )
                c += 1
        if not default and not timeV:
            estimatedGas = maindialoges.modifygasbelow(estimatedGas, connection)
        if timeV:
            medium_gas_aux = time.perf_counter() - start_time_perf
            medium_gas_time += medium_gas_aux
            start_time = time.perf_counter()
        transactions_hash = []
        for x in range(len(message_prepared)):
            #Calculate decoy Value
            random.seed(a=None)
            value = random.randint(1, 9) * random.choice([10000000000000,100000000000000])
            if (connection == "w3" and w3option == "infura") or (connection != "w3" and type(rpc) is not str):
                transactions_hash.append(w3mode.w3sendRawTransaction(senderAddress, key,('0x'+ message_prepared[x]), nonce, w3,gas=estimatedGas[x], value=hex(value)[2:]))
                nonce += 1
            else:
                if connection == "w3":
                    transactions_hash.append(w3mode.w3sendTransaction(senderAddress, key,('0x'+ message_prepared[x]),w3, option=w3option,gas=estimatedGas[x], value=value))
                else:
                    transactions_hash.append(rpcmode.rpcsendTransaction(senderAddress, ('0x'+ message_prepared[x]),rpc,c, gas = estimatedGas[x],value=hex(value)))
                    c += 1
        if timeV:
            medium_sen = time.perf_counter() - start_time
            medium_sending += medium_sen
            times_sending.append(medium_sen)
            start_time_perf = time.perf_counter()
        print("Transaction hashes",transactions_hash)
        if not default or timeV:
            if not timeV:
                timeout, wait = maindialoges.waitfortransaction()
            else:
                timeout = 700
                wait = True
            if wait:
                if connection == "w3" or type(rpc) is not str:
                    tx_receipts = w3mode.w3waitForTransactionReceipt(transactions_hash, w3, timeout)
                else:
                    tx_receipts = rpcmode.rpcwaitTransaction (transactions_hash, rpc, c, timeout=timeout)
                print("Transaction receipts",tx_receipts)
        if timeV:
            time_mining_perf = time.perf_counter() - start_time_perf
            medium_mining_perf += time_mining_perf
            times_mining.append(time_mining_perf)
            tx_hashes_list.append(transactions_hash)
            if connection == "w3" or type(rpc) is not str:
                medium_gas += sum(estimatedGas)
                gas_list.append(sum(estimatedGas))
            else:
                medium_gas += sum([ int(x,16) for x in estimatedGas ])
                gas_list.append(sum([ int(x,16) for x in estimatedGas ]))
            time.sleep(5)
    if timeV:
        time_mining_perf = medium_mining_perf/ntimes
        time_preparation_perf = medium_process_perf/ntimes
        medium_gas_time = medium_gas_time/ntimes
        medium_sending = medium_sending/ntimes
        medium_gas = medium_gas/ntimes
        transactions_hash = copy.deepcopy(tx_hashes_list)
        print("Medium gas spent:{} ".format(medium_gas))
        print("Gas spent per execution:{} ".format(gas_list))
        print("Message preparation time (average): {} seconds".format(time_preparation_perf))
        print("Message preparation time per execution : {} seconds".format(times_process))
        print("Sending to the blockchain time (average): {} seconds".format(medium_sending))
        print("Sending to the blockchain time per execution: {} seconds".format(times_sending))
        print("Mining time (average): {} seconds".format(time_mining_perf))
        print("Mining time per execution: {} seconds".format(times_mining))
    return transactions_hash
