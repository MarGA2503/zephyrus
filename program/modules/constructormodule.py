#!/usr/bin/env python
"""
Contains the main functions for introducing and retrieving the message from the
constructor
"""

import copy
import os
import time
from getpass import getpass
from ..connection import conaux
from ..auxiliar import messagefunctions
from ..auxiliar import getcheck
from ..auxiliar import maindialoges
from ..auxiliar import cipherfunctions
from ..auxiliar import globalsettings
from ..methods import calling

__author__ = "Mar Gimenez Aguilar"
__version__ = "1"
__maintainer__ = "Mar Gimenez Aguilar"
__email__ = "mar.gimeneza@gmail.com"

def extractContractConstructorMethod(tx_hashes,connection,ChaCha20key, password, default,ntimes, timeV):
    if connection == "w3":
        from ..connection import w3mode
        w3,w3option = conaux.connectWeb3()
    else:
        from ..connection import rpcmode
        rpc, w3 = conaux.connectRPC()
        c = 0

    used_path, contracts_path = maindialoges.extractcontractconstructor ()
    #First we retrieve the bytecode
    if timeV:
        medium_process_perf = 0
        time_block = 0
        times_process = []
        times_block = []
        original_hashes = copy.deepcopy(tx_hashes)
    for i in range(ntimes):
        if timeV:
            start_time_perf = time.perf_counter()
            tx_hashes = original_hashes[i]
            #print(tx_hashes)
        data_in_transactions = []
        #Extract the data from transactions
        nonces = []
        cipherfunctions.nonce = []
        last_block = 0
        for x in tx_hashes:
            if connection == "w3":
                result = w3mode.w3getTransaction([x],w3)
                data_in_transactions.append(result[0]['input'][2:])
                cipherfunctions.nonce.append(result[0]['nonce'])
                senderAddress = result[0]['from']
                if len(tx_hashes)== 1:
                    last_block = result[0]['blockNumber']
                #print(cipherfunctions.nonce)
            else:
                result = rpcmode.rpcgetTransaction([x], rpc, c)
                data_in_transactions.append(result[0]['result']['input'][2:])
                senderAddress = result[0]['result']['from']
                cipherfunctions.nonce.append(int(result[0]['result']['nonce'][2:],16))
                if len(tx_hashes)== 1:
                    last_block = int(result[0]['result']['blockNumber'][2:],16)
                c += 1
        if len(tx_hashes)== 1:
            if timeV:
                time_block_ = time.perf_counter() - start_time_perf
                time_block += time_block_
                time_block_execution = time_block_
                start_time_perf_retrieving = time.perf_counter()
            nonces = copy.deepcopy(cipherfunctions.nonce)
            if ChaCha20key == "":
                ChaCha20key = cipherfunctions.generateChaCha20key(cipherfunctions.strechKey(senderAddress.lower(),nonce=""))
            messages = []
            for x in data_in_transactions:
                end = getcheck.getendswarmhash(x)
                #This point to the last element of the swarmHash
                messages.append(x[(end):])
            print("Messages without bytecode",messages)
            #Now, to know how to extract the message, is necesary to know what the arguments are
            if used_path != "":
                arguments, sizes, dynamic = getcheck.getargumentsfromused(origin, used_path)
            #We need to extract the message now
            else:
                arguments, sizes, dynamic = getcheck.getargumentsfromcode(contracts_path, data_in_transactions,messages)
            contains_uint256 = [True for x in arguments if 'uint256' in x]
            if True in contains_uint256:
                contains_uint256 = contains_uint256[0]
            else:
                contains_uint256 = False
            #print("contains uint256", contains_uint256,data_in_transactions)
            #print(arguments, sizes, dynamic)
            list_sample = []
            list_zeros = []
            bits_per_length = 0
            dict_valuespossible = {}
            bits_per_length = {}
            if contains_uint256:
                if  sys.version_info >= (3, 6):
                    dict_valuespossible, bits_per_length =messagefunctions.preparelistpatternhigher36 ('constructor_uint256_lens', 'constructor_uint256_zeros_lens')
                else:
                    list_sample,list_zeros , dict_valuespossible, bits_per_length = messagefunctions.preparelistpatternbelow36 ('constructor_uint256_lens', 'constructor_uint256_zeros_lens')
            messages_after = []
            nonces = copy.deepcopy(cipherfunctions.nonce)
            cipherfunctions.nonce = copy.deepcopy(nonces)
            ChaCha20keycopy = copy.deepcopy(ChaCha20key)
            for x in range(len(messages)):
                messages_after.append(calling.readcall (arguments[x], messages[x], sizes[x], dynamic[x], dict_valuespossible,ChaCha20key))
                #print(messages_after)
            print("Message extracted from arguments", messages_after)
            cipherfunctions.nonce = copy.deepcopy(nonces)
            nonces = copy.deepcopy(cipherfunctions.nonce)
            cipherfunctions.nonce = copy.deepcopy(nonces)
            nonce_first = copy.deepcopy(nonces)[0]
            print("First nonce", nonce_first)
            #single, data_in_transactions, total,index,withmessage = messagefunctions.checkifsingle(messages_after, ChaCha20key)
            print("Chacha",ChaCha20keycopy )
            len_of_length, total_length, message, in_next_transaction,first_low_length = messagefunctions.checkifsinglebytecodeMethod (messages_after,1,ChaCha20keycopy)
            #print("Chacha",ChaCha20keycopy )
            #print("extracted", len_of_length, total_length, message, in_next_transaction,first_low_length)
            if not in_next_transaction and len(message)>= (total_length * 8):
                single = True
                print("Single!!", total_length, message)
                message = message[:(total_length * 8)]
            else:
                single = False
            # while not withmessage:
            #     while last_block > 0 and 0 nonce:
            #         if connection == "w3":
            #             data_to_retrieve, nonces_tx = w3mode.w3getBlocktransactionsbyNumber (last_block, 'from', senderAddress, 'to', w3)
            #If part of a larger message
            if timeV:
                time_extraction_perf = time.perf_counter() - start_time_perf_retrieving
                medium_process_perf += time_extraction_perf
                time_process_execution = time_extraction_perf
            if not single:
                cumulative_nonces= []
                cumulative_nonces.append(nonces[0])
                transactions_found = 1
                block_in_use = copy.deepcopy(last_block)
                all_extracted = False
                while not all_extracted:
                    if timeV:
                        start_time_perf_block = time.perf_counter()
                    if connection == "w3":
                        data_to_retrieve, nonces_tx = w3mode.w3getBlocktransactionsbyNumber (block_in_use, ['from','to'], ['== ' + str(senderAddress), '== ' + str(None)], 'input', w3)
                    else:
                        data_to_retrieve, nonces_tx = rpcmode.rpcgetBlocktransactionsbyNumber (hex(block_in_use), ['from','to'], ['== ' + str(senderAddress), '== ' + str(None)], 'input', rpc,c)
                        c += 1
                    if timeV:
                        time_block_ = time.perf_counter() - start_time_perf_block
                        time_block += time_block_
                        time_block_execution += time_block_
                        start_time_perf_retrieving = time.perf_counter()
                    #print(nonces_tx, data_to_retrieve)
                    if nonce_first in nonces_tx:
                        index_nonce = nonces_tx.index(nonce_first)
                        #del nonces_tx[index_nonce]
                        del data_to_retrieve[index_nonce]
                    #print(nonces_tx, data_to_retrieve)
                    if len(data_to_retrieve) > 0:
                        cipherfunctions.nonce = copy.deepcopy(nonces_tx)
                        messages = []
                        nonces = copy.deepcopy([cipherfunctions.nonce])
                        ChaCha20keycopy = copy.deepcopy(ChaCha20key)
                        dynamic = []
                        sizes = []
                        for x in data_to_retrieve:
                            end = getcheck.getendswarmhash(x)
                            #This point to the last element of the swarmHash
                            messages.append(x[(end):])
                        #print("Messages without bytecode",messages)
                        #Now, to know how to extract the message, is necesary to know what the arguments are
                        if used_path != "":
                            arguments, sizes, dynamic = getcheck.getargumentsfromused(origin, used_path)
                        #We need to extract the message now
                        else:
                            arguments, sizes, dynamic = getcheck.getargumentsfromcode(contracts_path, data_to_retrieve,messages)
                        #print(arguments, sizes, dynamic)
                        contains_uint256 = [True for x in arguments if 'uint256' in x]
                        if True in contains_uint256:
                            contains_uint256 = contains_uint256[0]
                        else:
                            contains_uint256 = False
                        #print("contains uint256", contains_uint256, sizes)
                        #print(arguments, sizes, dynamic)
                        list_sample = []
                        list_zeros = []
                        bits_per_length = 0
                        dict_valuespossible = {}
                        bits_per_length = {}
                        if contains_uint256:
                            if  sys.version_info >= (3, 6):
                                dict_valuespossible, bits_per_length =messagefunctions.preparelistpatternhigher36 ('constructor_uint256_lens', 'constructor_uint256_zeros_lens')
                            else:
                                list_sample,list_zeros , dict_valuespossible, bits_per_length = messagefunctions.preparelistpatternbelow36 ('constructor_uint256_lens', 'constructor_uint256_zeros_lens')
                        messages_after = []
                        for x in range(len(messages)):
                            messages_after.append(calling.readcall (arguments[x], messages[x], sizes[x], dynamic[x],dict_valuespossible,ChaCha20key))
                            #print(messages_after)
                        print("Message extracted from arguments", messages_after)
                        print("messages", messages)
                        print("in next", in_next_transaction)
                        print("Chacha",ChaCha20key )
                        cipherfunctions.nonce = copy.deepcopy([nonce_first])
                        message,len_of_length, total_length, message, in_next_transaction,all_extracted,first_low_length= messagefunctions.checkextractbytecode(messages_after,1,len_of_length, total_length, message, in_next_transaction,first_low_length, ChaCha20keycopy)
                    block_in_use += 1
                    transactions_found += len(messages)
                    print("Transactions recovered: ", transactions_found)
                    print("Block: ", block_in_use)
                    if timeV:
                        time_extraction_perf = time.perf_counter() - start_time_perf_retrieving
                        medium_process_perf += time_extraction_perf
                        #print("añado")
                        time_process_execution += time_extraction_perf
                nonces = copy.deepcopy(cumulative_nonces)
            if timeV:
                #print("LLego")
                times_process.append(time_process_execution)
                times_block.append(time_block_execution)
                # cumulative_nonces = [-1] * total
                # cumulative_nonces[(index-1)] = nonces[0]
                # transactions_found = 0
                # block_in_use = copy.deepcopy(last_block)
                # while transactions_found < total:
                #     #Check if we have to retrieve in newer blocks, oldest or both
                #     #If the it the last transaction, we look for transactions in the current block
                #     # and older blocks
                #     if index == total:
                #         if timeV:
                #             start_time_perf_block = time.perf_counter()
                #         if connection == "w3":
                #             data_to_retrieve, nonces_tx = w3mode.w3getBlocktransactionsbyNumber (block_in_use, ['from','to'], ['== ' + str(senderAddress), '== ' + str(None)], 'input', w3)
                #         else:
                #             data_to_retrieve, nonces_tx = rpcmode.rpcgetBlocktransactionsbyNumber (hex(block_in_use), ['from','to'], ['== ' + str(senderAddress), '== ' + str(None)], 'input', rpc,c)
                #             c += 1
                #         if timeV:
                #             time_block_ = time.perf_counter() - start_time_perf_block
                #             time_block += time_block_
                #             time_block_execution += time_block_
                #             start_time_perf_retrieving = time.perf_counter()
                #         cipherfunctions.nonce = copy.deepcopy(nonces_tx)
                #         messages = []
                #         for x in data_to_retrieve:
                #             end = getcheck.getendswarmhash(x)
                #             #This point to the last element of the swarmHash
                #             messages.append(x[(end):])
                #         #print("Messages without bytecode",messages)
                #         #Now, to know how to extract the message, is necesary to know what the arguments are
                #         if used_path != "":
                #             arguments, sizes, dynamic = getcheck.getargumentsfromused(origin, used_path)
                #         #We need to extract the message now
                #         else:
                #             arguments, sizes, dynamic = getcheck.getargumentsfromcode(contracts_path, data_to_retrieve,messages)
                #         print(arguments, sizes, dynamic)
                #         messages_after = []
                #         for x in range(len(messages)):
                #             messages_after.append(calling.readcall (arguments[x], messages[x], sizes[x], dynamic[x],cipherfunctions.upgradeChaCha20key(ChaCha20key)))
                #             #print(messages_after)
                #         #print("Message extracted from arguments", messages_after)
                #         cipherfunctions.nonce = copy.deepcopy(nonces_tx)
                #         nonces = copy.deepcopy(cipherfunctions.nonce)
                #         data_in_transactions ,cumulative_nonces, added = messagefunctions.checkifpartofmessage (messages_after, nonces_tx, data_in_transactions, cumulative_nonces, ChaCha20key)
                #         block_in_use -= 1
                #         transactions_found += added
                #         if timeV:
                #             time_extraction_perf = time.perf_counter() - start_time_perf_retrieving
                #             medium_process_perf += time_extraction_perf
                #             time_process_execution += time_extraction_perf
                #     elif index == 1:
                #         if timeV:
                #             start_time_perf_block = time.perf_counter()
                #         if connection == "w3":
                #             data_to_retrieve, nonces_tx = w3mode.w3getBlocktransactionsbyNumber (block_in_use, ['from','to'], ['== ' + str(senderAddress), '== ' + str(None)], 'input', w3)
                #         else:
                #             data_to_retrieve, nonces_tx = rpcmode.rpcgetBlocktransactionsbyNumber (hex(block_in_use), ['from','to'], ['== ' + str(senderAddress), '== ' + str(None)], 'input', rpc,c)
                #             c += 1
                #         if timeV:
                #             time_block_ = time.perf_counter() - start_time_perf_block
                #             time_block += time_block_
                #             time_block_execution += time_block_
                #             start_time_perf_retrieving = time.perf_counter()
                #         cipherfunctions.nonce = copy.deepcopy(nonces_tx)
                #         messages = []
                #         for x in data_to_retrieve:
                #             end = getcheck.getendswarmhash(x)
                #             #This point to the last element of the swarmHash
                #             messages.append(x[(end):])
                #         #print("Messages without bytecode",messages)
                #         #Now, to know how to extract the message, is necesary to know what the arguments are
                #         if used_path != "":
                #             arguments, sizes, dynamic = getcheck.getargumentsfromused(origin, used_path)
                #         #We need to extract the message now
                #         else:
                #             arguments, sizes, dynamic = getcheck.getargumentsfromcode(contracts_path, data_to_retrieve,messages)
                #         #print(arguments, sizes, dynamic)
                #         messages_after = []
                #         for x in range(len(messages)):
                #             messages_after.append(calling.readcall (arguments[x], messages[x], sizes[x], dynamic[x],cipherfunctions.upgradeChaCha20key(ChaCha20key)))
                #             #print(messages_after)
                #         #print("Message extracted from arguments", messages_after)
                #         cipherfunctions.nonce = copy.deepcopy(nonces_tx)
                #         nonces = copy.deepcopy(cipherfunctions.nonce)
                #         data_in_transactions ,cumulative_nonces, added = messagefunctions.checkifpartofmessage (messages_after, nonces_tx, data_in_transactions, cumulative_nonces, ChaCha20key)
                #         block_in_use += 1
                #         transactions_found += added
                #         if timeV:
                #             time_extraction_perf = time.perf_counter() - start_time_perf_retrieving
                #             medium_process_perf += time_extraction_perf
                #             time_process_execution += time_extraction_perf
                #     else:
                #         while cumulative_nonces[0] == -1:
                #             if timeV:
                #                 start_time_perf_block = time.perf_counter()
                #             #Look for the transactions in older blocks including
                #             #the current one
                #             if connection == "w3":
                #                 data_to_retrieve, nonces_tx = w3mode.w3getBlocktransactionsbyNumber (block_in_use, ['from','to'], ['== ' + str(senderAddress), '== ' + str(None)], 'input', w3)
                #             else:
                #                 data_to_retrieve, nonces_tx = rpcmode.rpcgetBlocktransactionsbyNumber (hex(block_in_use), ['from','to'], ['== ' + str(senderAddress), '== ' + str(None)], 'input', rpc,c)
                #                 c += 1
                #             if timeV:
                #                 time_block_ = time.perf_counter() - start_time_perf_block
                #                 time_block += time_block_
                #                 time_block_execution += time_block_
                #                 start_time_perf_retrieving = time.perf_counter()
                #             cipherfunctions.nonce = copy.deepcopy(nonces_tx)
                #             messages = []
                #             for x in data_to_retrieve:
                #                 end = getcheck.getendswarmhash(x)
                #                 #This point to the last element of the swarmHash
                #                 messages.append(x[(end):])
                #             #print("Messages without bytecode",messages)
                #             #Now, to know how to extract the message, is necesary to know what the arguments are
                #             if used_path != "":
                #                 arguments, sizes, dynamic = getcheck.getargumentsfromused(origin, used_path)
                #             #We need to extract the message now
                #             else:
                #                 arguments, sizes, dynamic = getcheck.getargumentsfromcode(contracts_path, data_to_retrieve,messages)
                #             #print(arguments, sizes, dynamic)
                #             messages_after = []
                #             for x in range(len(messages)):
                #                 messages_after.append(calling.readcall (arguments[x], messages[x], sizes[x], dynamic[x],cipherfunctions.upgradeChaCha20key(ChaCha20key)))
                #                 #print(messages_after)
                #             #print("Message extracted from arguments", messages_after)
                #             cipherfunctions.nonce = copy.deepcopy(nonces_tx)
                #             nonces = copy.deepcopy(cipherfunctions.nonce)
                #             data_in_transactions ,cumulative_nonces, added = messagefunctions.checkifpartofmessage (messages_after, nonces_tx, data_in_transactions, cumulative_nonces, ChaCha20key)
                #             block_in_use -= 1
                #             transactions_found += added
                #             if timeV:
                #                 time_extraction_perf = time.perf_counter() - start_time_perf_retrieving
                #                 medium_process_perf += time_extraction_perf
                #                 time_process_execution += time_extraction_perf
                #         block_in_use = copy.deepcopy(last_block) + 1
                #         while cumulative_nonces[-1] == -1:
                #             if timeV:
                #                 start_time_perf_block = time.perf_counter()
                #             #Look for the transactions in newer blocks excluding
                #             # the block of the original transaction
                #             if connection == "w3":
                #                 data_to_retrieve, nonces_tx = w3mode.w3getBlocktransactionsbyNumber (block_in_use, ['from','to'], ['== ' + str(senderAddress), '== ' + str(None)], 'input', w3)
                #             else:
                #                 data_to_retrieve, nonces_tx = rpcmode.rpcgetBlocktransactionsbyNumber (hex(block_in_use), ['from','to'], ['== ' + str(senderAddress), '== ' + str(None)], 'input', rpc,c)
                #                 c += 1
                #             if timeV:
                #                 time_block_ = time.perf_counter() - start_time_perf_block
                #                 time_block += time_block_
                #                 time_block_execution += time_block_
                #                 start_time_perf_retrieving = time.perf_counter()
                #             cipherfunctions.nonce = copy.deepcopy(nonces_tx)
                #             messages = []
                #             for x in data_to_retrieve:
                #                 end = getcheck.getendswarmhash(x)
                #                 #This point to the last element of the swarmHash
                #                 messages.append(x[(end):])
                #             #print("Messages without bytecode",messages)
                #             #Now, to know how to extract the message, is necesary to know what the arguments are
                #             if used_path != "":
                #                 arguments, sizes, dynamic = getcheck.getargumentsfromused(origin, used_path)
                #             #We need to extract the message now
                #             else:
                #                 arguments, sizes, dynamic = getcheck.getargumentsfromcode(contracts_path, data_to_retrieve,messages)
                #             #print(arguments, sizes, dynamic)
                #             messages_after = []
                #             for x in range(len(messages)):
                #                 messages_after.append(calling.readcall (arguments[x], messages[x], sizes[x], dynamic[x],cipherfunctions.upgradeChaCha20key(ChaCha20key)))
                #                 #print(messages_after)
                #             #print("Message extracted from arguments", messages_after)
                #             cipherfunctions.nonce = copy.deepcopy(nonces_tx)
                #             nonces = copy.deepcopy(cipherfunctions.nonce)
                #             data_in_transactions ,cumulative_nonces, added = messagefunctions.checkifpartofmessage (messages_after, nonces_tx, data_in_transactions, cumulative_nonces, ChaCha20key)
                #             block_in_use += 1
                #             transactions_found += added
                #             if timeV:
                #                 time_extraction_perf = time.perf_counter() - start_time_perf_retrieving
                #                 medium_process_perf += time_extraction_perf
                #                 time_process_execution += time_extraction_perf
                # nonces = copy.deepcopy(cumulative_nonces)
                # if timeV:
                #     times_process.append(time_process_execution)
                #     times_block.append(time_block_execution)
        #print("Data in transactions",data_in_transactions)
        if len(tx_hashes) > 1:
            print("Introduce the first transaction hash\n")
            exit(1)
            # if timeV:
            #     time_block_ = time.perf_counter() - start_time_perf
            #     time_block += time_block_
            #     time_block_execution = time_block_
            #     start_time_perf = time.perf_counter()
            # nonces = copy.deepcopy(cipherfunctions.nonce)
            # if ChaCha20key == "":
            #     ChaCha20key = cipherfunctions.generateChaCha20key(cipherfunctions.strechKey(senderAddress.lower(),nonce=""))
            # #Now we need to extract the constructor arguments
            # messages = []
            # for x in data_in_transactions:
            #     end = getcheck.getendswarmhash(x)
            #     #This point to the last element of the swarmHash
            #     messages.append(x[(end):])
            # print("Messages without bytecode",messages)
            # #Now, to know how to extract the message, is necesary to know what the arguments are
            # if used_path != "":
            #     arguments, sizes, dynamic = getcheck.getargumentsfromused(origin, used_path)
            # #We need to extract the message now
            # else:
            #     arguments, sizes, dynamic = getcheck.getargumentsfromcode(contracts_path, data_in_transactions,messages)
            # print(arguments, sizes, dynamic)
            # messages_after = []
            # for x in range(len(messages)):
            #     messages_after.append(calling.readcall (arguments[x], messages[x], sizes[x], dynamic[x],cipherfunctions.upgradeChaCha20key(ChaCha20key)))
            #     print(messages_after)
            # #print("Message extracted from arguments", messages_after)
            # cipherfunctions.nonce = copy.deepcopy(nonces)
            # data_in_transactions, nonces = messagefunctions.orderblocks(messages_after,ChaCha20key,nonces)
            # if timeV:
            #     #times_process.append(time_process_execution)
            #     times_block.append(time_block_execution)
            #     times_process.append(0)
        if timeV and len(tx_hashes) == 1:
            start_time_perf = time.perf_counter()
        #cipherfunctions.nonce = copy.deepcopy(nonces)
        # if password != "":
        #     for x in data_in_transactions:
        #         data_in_transactions = ordermessage(x[2:], password)
        #message = messagefunctions.extractmessagehex(data_in_transactions, 0, "extractContractConstructorMethod", ChaCha20key, password=password)
        if timeV:
            time_extraction_perf = time.perf_counter() - start_time_perf
            medium_process_perf += time_extraction_perf
            times_process[-1] += time_extraction_perf
    #print("Message", message)
    message = bytearray([int(message[i:i+8], 2) for i in range(0, len(message), 8)]).hex()
    print("Message", message)
    if timeV:
        time_extraction_perf = medium_process_perf/ntimes
        time_block = time_block/ntimes
        print("Retrieving from the blockchain time (average): {} seconds".format(time_block))
        print("Retrieving from the blockchain time per execution: {} seconds".format(times_block))
        print("Revealing time (average): {} seconds".format(time_extraction_perf))
        print("Revealing time per execution: {} seconds ".format(times_process))
    return message


def ContractConstructorMethod(data,connection,ChaCha20key, password, default,ntimes, timeV):
    #print(connection)
    if connection == "w3":
        from ..connection import w3mode
        w3,w3option = conaux.connectWeb3()
    else:
        from ..connection import rpcmode
        rpc, w3 = conaux.connectRPC()
        c = 0
    #We have to get the variable id web3
    #TODO: allow dirs
    print("Introduce the sender address\n")
    senderAddress = input()
    key = ""
    if (connection == "w3" and w3option == "infura") or (connection != "w3" and type(w3) is not str):
        if type(w3) is not str:
            from ..connection import w3mode
            rpc = w3
            w3option = "infura"
        print("Introduce the sender private key\n")
        key = getpass()
    elif connection == "w3" and w3option != "HTTP":
        print("Introduce the sender password\n")
        key = getpass()
    print("Please introduce the path to de .abi\n")
    Correct = False
    while  not Correct:
        origin = input()
        if os.path.lexists(origin):
            Correct = True
        else:
            print("Path not valid\n")
    if timeV:
        medium_process_perf = 0
        medium_mining_perf = 0
        times_process1 = []
    for i in range(ntimes):
        if timeV:
            start_time_perf = time.perf_counter()
        if ChaCha20key == "":
            ChaCha20key = cipherfunctions.generateChaCha20key(cipherfunctions.strechKey(senderAddress.lower(),nonce=""))
        if connection == "w3" or type(rpc) is not str:
            cipherfunctions.nonce = w3mode.w3getAccountNonce(senderAddress,w3)
            nonce = w3mode.w3getAccountNonce(senderAddress,w3)
        else:
            cipherfunctions.nonce = int(rpcmode.rpcgetAccountNonce(senderAddress,rpc,c),16)
            c += 1
        contracts = getcheck.getContracts(origin)
        if timeV:
            time_preparation_perf = time.perf_counter() - start_time_perf
            medium_process_perf += time_preparation_perf
            times_process1.append(time_preparation_perf)
    if timeV:
        time_preparation_perf1 = medium_process_perf/ntimes
    arguments, sizes, dynamic, contract_index, original_arguments = getcheck.checkcontractsconstructor(contracts, default)
    print(arguments, sizes, dynamic, contract_index)
    if timeV:

        # if  len(dynamic[-1]) > 0:
        #     number = 1
        #     if default:
        #         form = '00'
        #         lengthsmin = []
        #     else:
        #         print("Do you want to distribute the message weight between the arguments?(y/n)\n")
        #         Correct = False
        #         while not Correct:
        #             option = input()
        #             if option == "y" or option == "yes":
        #                 form = '01'
        #                 Correct = True
        #             elif option == "n" or option == "no":
        #                 form = '00'
        #                 Correct = True
        #             else:
        #                 print ("Option not valid\n")
        # else:
        #     if default:
        #          form = '10'
        #     else:
        #         if form == "":
        #             print("Do you want to distribute the message weight among contracts?(y/n)\n")
        #             Correct = False
        #             while not Correct:
        #                 option = input()
        #                 if option == "y" or option == "yes":
        #                     form = '0'
        #                     Correct = True
        #                 elif option == "n" or option == "no":
        #                     form = '1'
        #                     Correct = True
        #                 else:
        #                     print ("Option not valid\n")
        #             if form == '0':
        #                 print("Introduce the Introduce the approximate size (bytes) to use per contract\n")
        #                 Correct = False
        #                 while not Correct:
        #                     number = int(input())*2
        #                     if number <= globalsettings.max_number_transactions and number >=2:
        #                         Correct = True
        #                     else:
        #                         print ("Option not valid. It must be a number between 2 and 255 \n")
        #             print("Do you want to distribute the message weight between the arguments?(y/n)\n")
        #             Correct = False
        #             while not Correct:
        #                 option = input()
        #                 if option == "y" or option == "yes":
        #                     form += '1'
        #                     Correct = True
        #                 elif option == "n" or option == "no":
        #                     form += '0'
        #                     Correct = True
        #                 else:
        #                     print ("Option not valid\n")
        medium_process_perf = 0
        medium_mining_perf = 0
        medium_sending= 0
        medium_gas_time = 0
        medium_gas= 0
        gas_list = []
        times_sending = []
        times_process2 = []
        times_mining = []
        tx_hashes_list = []
    for i in range(ntimes):
        if timeV:
            start_time_perf = time.perf_counter()
        if connection == "w3" or type(rpc) is not str:
            cipherfunctions.nonce = w3mode.w3getAccountNonce(senderAddress,w3)
            nonce = w3mode.w3getAccountNonce(senderAddress,w3)
        else:
            cipherfunctions.nonce = int(rpcmode.rpcgetAccountNonce(senderAddress,rpc,c),16)
            c += 1
        #print(arguments, sizes, dynamic, original_arguments)
        #contains_uint256 = [True for x in arguments if 'uint256' in x][0]
        contains_uint256 = [True for x in arguments if 'uint256' in x]
        if True in contains_uint256:
            contains_uint256 = contains_uint256[0]
        else:
            contains_uint256 = False
        #print("contains uint256", contains_uint256)
        list_sample = []
        list_zeros = []
        bits_per_length = 0
        dict_valuespossible = {}
        bits_per_length = {}
        form = '00'
        if contains_uint256:
            if  sys.version_info >= (3, 6):
                dict_valuespossible, bits_per_length =messagefunctions.preparelistpatternhigher36 ('constructor_uint256_lens', 'constructor_uint256_zeros_lens')
            else:
                list_sample,list_zeros , dict_valuespossible, bits_per_length = messagefunctions.preparelistpatternbelow36 ('constructor_uint256_lens', 'constructor_uint256_zeros_lens')
        number = 1
        if timeV:
            message_prepared, form, lengthsmin = messagefunctions.preparemessageconstruccall(data, sizes, dynamic,password,ChaCha20key,default,form=form, number=number)
        else:
            message_prepared, form, lengthsmin = messagefunctions.preparemessageconstruccall(data, sizes, dynamic,password,ChaCha20key,default)
        # if form[-1] == '1':
        #     rea = True
        # else:
        #     rea = False
        rea = False
        print("Message prepared",message_prepared)
        constructor_arguments = []
        used = []
        if os.path.isdir(origin):
            names = [x for x in sorted(os.listdir(origin)) if x[-4:] == '.abi']
        else:
            names = [origin]
        if contract_index != -1:
            used.append(names[contract_index])
            contracts = [contracts[contract_index]]
        mantainsize = -1
        for i in range(len(message_prepared)):
            dic = {}
            # if len(arguments[(i%len(arguments))]) != 0:
            #     if i == (len(message_prepared) - 1):
            #         if len(message_prepared) > 2:
            #             if len(message_prepared[i]) < len(message_prepared[i-1]) and len(message_prepared[i]) < len(message_prepared[i-2]):
            #                 mantainsize = len(message_prepared[i-2])
            #         elif len(message_prepared) > 1:
            #             if len(message_prepared[i]) < len(message_prepared[i-1]):
            #                 mantainsize = len(message_prepared[i-1])
            # sizesn = calling.dividemessage (arguments[(i%len(arguments))], message_prepared[i], sizes[(i%len(sizes))], dynamic[(i%len(dynamic))], rea)
            # sizesnn = calling.checkleninfirst(sizes[(i%len(sizes))],sizesn, lengthsmin[(i%len(lengthsmin))])
            # print(sizesn,sizesnn)

            #cons_args = calling.createcall (arguments[(i%len(arguments))], message_prepared[i], sizes[(i%len(sizes))], dynamic[(i%len(dynamic))], lengthsmin[(i%len(lengthsmin))], rea,dict_valuespossible, 'constructor_uint256_lens', 'constructor_uint256_zeros_lens', list_sample,list_zeros, mantainsize = mantainsize)
            cons_args = calling.createcall (arguments[(i%len(arguments))], message_prepared[i], sizes[(i%len(sizes))], dynamic[(i%len(dynamic))], dict_valuespossible, 'constructor_uint256_lens', 'constructor_uint256_zeros_lens', list_sample,list_zeros,rea, mantainsize = mantainsize)
            print("Message in arguments",cons_args)
            #print(contracts)
            #print(contracts[(i%len(contracts))]['bin'])
            dic['bin'] = contracts[(i%len(contracts))]['bin'] + cons_args
            if contract_index == -1:
                used.append(names[(i%len(names))])
            constructor_arguments.append(dic)
        #print(constructor_arguments, used)
        # estimatedGas = getEstimatedGasCost(message_prepared, connection, sendTransaction)
        estimatedGas = []
        if timeV:
            time_preparation_perf = time.perf_counter() - start_time_perf
            medium_process_perf += time_preparation_perf
            times_process2.append(time_preparation_perf)
            start_time_perf = time.perf_counter()
        #print(connection)
        for x in constructor_arguments:
            #print(x)
            if connection == "w3" or type(rpc) is not str:
                estimatedGas.append(w3mode.w3estimateGasdeployContractConstructor(senderAddress, key,x,w3,option=w3option))
            else:
                estimatedGas.append(rpcmode.rpcestimateGasdeployContract(senderAddress,('0x'+ x['bin']) ,rpc,c) )
                c += 1
        if not default and not timeV:
            estimatedGas = maindialoges.modifygasbelow(estimatedGas, connection)
        transactions_hash = []
        if timeV:
            medium_gas_aux = time.perf_counter() - start_time_perf
            medium_gas_time += medium_gas_aux
            start_time = time.perf_counter()
        for x in range(len(constructor_arguments)):
            if (connection == "w3" and w3option == "infura") or (connection != "w3" and type(rpc) is not str):
                transactions_hash.append(w3mode.w3deployRawContract(senderAddress, key, (constructor_arguments[x]), nonce, w3, gas=estimatedGas[x]))
                nonce += 1
            else:
                if connection == "w3":
                    transactions_hash.append(w3mode.w3deployContractConstructor(senderAddress, key,(constructor_arguments[x]),w3, option=w3option,gas=estimatedGas[x]))
                else:
                    transactions_hash.append(rpcmode.rpcdeployContract(senderAddress, ('0x'+ constructor_arguments[x]['bin']),rpc,c, gas = estimatedGas[x]))
                    c += 1
        if timeV:
            medium_sen = time.perf_counter() - start_time
            medium_sending += medium_sen
            times_sending.append(medium_sen)
            start_time_perf = time.perf_counter()
        print("Transaction hashes",transactions_hash)
        if not default or timeV:
            if not timeV:
                timeout, wait = maindialoges.waitfortransaction()
            else:
                timeout = 700
                wait = True
            if wait:
                if connection == "w3" or type(rpc) is not str:
                    tx_receipts = w3mode.w3waitForTransactionReceipt(transactions_hash, w3, timeout)
                else:
                    tx_receipts = rpcmode.rpcwaitTransaction (transactions_hash, rpc, c, timeout=timeout)
                print("Transaction receipts",tx_receipts)
            if timeV:
                time_mining_perf = time.perf_counter() - start_time_perf
                medium_mining_perf += time_mining_perf
                times_mining.append(time_mining_perf)
                tx_hashes_list.append(transactions_hash)
                if connection == "w3" or type(rpc) is not str:
                    medium_gas += sum(estimatedGas)
                    gas_list.append(sum(estimatedGas))
                else:
                    medium_gas += sum([ int(x,16) for x in estimatedGas ])
                    gas_list.append(sum([ int(x,16) for x in estimatedGas ]))
                time.sleep(5)
        with open(os.path.join(".", "used.txt"), "w") as outfile:
            for x in used:
                outfile.write(x+ "\n")
        outfile.close()
    if timeV:
        time_mining_perf = medium_mining_perf/ntimes
        time_preparation_perf = medium_process_perf/ntimes
        medium_gas_time = medium_gas_time/ntimes
        medium_sending = medium_sending/ntimes
        medium_gas = medium_gas/ntimes
        transactions_hash = copy.deepcopy(tx_hashes_list)
        times_process = [sum(x) for x in zip(times_process1, times_process2)]
        print("Medium gas spent:{} ".format(medium_gas))
        print("Gas spent per execution:{} ".format(gas_list))
        print("Message preparation time (average): {} seconds".format(time_preparation_perf))
        print("Message preparation time per execution : {} seconds".format(times_process))
        print("Sending to the blockchain time (average): {} seconds".format(medium_sending))
        print("Sending to the blockchain time per execution: {} seconds".format(times_sending))
        print("Mining time (average): {} seconds".format(time_mining_perf))
        print("Mining time per execution: {} seconds".format(times_mining))
    return transactions_hash
