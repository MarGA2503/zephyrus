#!/usr/bin/env python
"""
Contains all functions related with the swarm hash method
"""

import random
import cbor

__author__ = "Mar Gimenez Aguilar"
__version__ = "1"
__maintainer__ = "Mar Gimenez Aguilar"
__email__ = "mar.gimeneza@gmail.com"

# def findswarm (bytecode):
#     #Find bzzr (in  case of const arguments, we dont know when metadata ends)
#     index_bzzr = bytecode.rfind('627a7a72')
#     #We know two bytes before:
#     index_metadata = index_bzzr -4
#     metadata = cbor.loads(bytes.fromhex(bytecode[index_metadata:]))
#     return index_metadata, metadata


def findswarm (bytecode):
    #Find bzzr (in  case of const arguments, we dont know when metadata ends)
    index_bzzr = bytecode.rfind('627a7a72')
    count = bytecode.count("627a7a72")
    #We know two bytes before:
    index_metadata = -1
    metadata = 0
    found = False
    while count > 0 and not found:
        index_metadata = index_bzzr -4
        try:
            metadata = cbor.loads(bytes.fromhex(bytecode[index_metadata:]))
            #print(metadata)
            if metadata!= 0:
                found = True
            else:
                index_bzzr = bytecode[:index_bzzr].rfind('627a7a72')
        except ValueError:
            index_bzzr = bytecode[:index_bzzr].rfind('627a7a72')
        #print(index_bzzr)
        count -= 1
    return index_metadata, metadata

def findipfs (bytecode):
    #Find bzzr (in  case of const arguments, we dont know when metadata ends)
    index_bzzr = bytecode.rfind('69706673')
    count = bytecode.count("69706673")
    #We know two bytes before:
    index_metadata = -1
    metadata = 0
    found = False
    while count > 0 and not found:
        index_metadata = index_bzzr -4
        try:
            metadata = cbor.loads(bytes.fromhex(bytecode[index_metadata:]))
            #print(metadata)
            if metadata!= 0:
                found = True
            else:
                index_bzzr = bytecode[:index_bzzr].rfind('69706673')
        except ValueError:
            index_bzzr = bytecode[:index_bzzr].rfind('69706673')
        #print(index_bzzr)
        count -= 1
    return index_metadata, metadata

def iSwarmHash (bytecode, message, key = ""):
    """
    Introduces a message in the format of a hex string in the swarm hash of the
    original bytecode passed as argument
    """
    if (len(message) > 64):
        print("The message is not the right length")
        exit(1)
    elif (len(message) < 64):
        rest = 64 - len(message)
        aux = ''.join([hex(random.randint(0,15))[2:] for i in range(rest)])
        #print(aux)
        message += aux
        #print("m", message)
    if key != "":
        random.seed(key)
        orderMessage = random.sample(list(range(0,64)),64)
        newSwarmHash = ""
        for i in orderMessage:
            newSwarmHash += message[i]
    else:
        newSwarmHash = message
    #Old method
    #newBytecode = bytecode[:-68] + newSwarmHash + bytecode[-4:]
    index_metadata, metadata = findswarm (bytecode)
    #We look for the bzzr and update the key in the dict:
    #print(metadata, index_metadata)
    bzzr = False
    for key in metadata:
        if 'bzzr' in key:
            metadata[key] = bytes.fromhex(message)
            bzzr = True
    if not bzzr:
        index_metadata, metadata = findipfs (bytecode)
        for key in metadata:
            if 'ipfs' in key:
                metadata[key] = '1220' + bytes.fromhex(message)
    #Dump in cbor
    #print(metadata,message)
    new_metadata = cbor.dumps(metadata).hex()
    #print(bytecode)
    newBytecode = bytecode[:index_metadata] + new_metadata + bytecode[(index_metadata+ len(new_metadata)):]
    #print(newBytecode,bytecode[:index_metadata], new_metadata, bytecode[(index_metadata+ len(new_metadata)):] )
    return newBytecode

def dSwarmHash (bytecode, key = ""):
    """
    Extract the message in the format of a hex string from the swarm hash of the
    bytecode passed as argument
    """
    if key != "":
        random.seed(key)
        orderMessage = random.sample(list(range(0,64)),64)
        #swarmHash = bytecode[-68:-4]
        index_metadata, metadata = findswarm (bytecode)
        bzzr = False
        for key in metadata:
            if 'bzzr' in key:
                swarmHash = metadata[key].hex()

        if not bzzr:
            index_metadata, metadata = findipfs (bytecode)
            for key in metadata:
                if 'ipfs' in key:
                    swarmHash = metadata[key].hex()[-64:]
        #print(swarmHash)
        message = list(swarmHash)
        #print(message)
        x = 0
        for i in orderMessage:
            message[i] = swarmHash[x]
            x += 1
        return ''.join(message)
    else:
        index_metadata, metadata = findswarm (bytecode)
        for key in metadata:
            if key.startswith('bzzr'):
                swarmHash = metadata[key].hex()
        #print(swarmHash)
        return swarmHash


# bytecode = "6060604052341561000f57600080fd5b60ba8061001d6000396000f300606060405260043610603f576000357c0100000000000000000000000000000000000000000000000000000000900463ffffffff168063771602f7146044575b600080fd5b3415604e57600080fd5b606b60048080359060200190919080359060200190919050506081565b6040518082815260200191505060405180910390f35b60008183019050929150505600a165627a7a72305820b7a61b947591d96eac948534743041ba7f399c72e20a6a802a88e0a24ab71e120029"
# message = "4c6f72656d20697073756d20646f6c6f722073697420616d657420616d65742e"
# newBytecode = iSwarmHash ( bytecode, message, key = "hola")
# print(newBytecode)
# emessage = dSwarmHash(newBytecode, key = "hola")
# print(emessage)
# assert (message == emessage)
