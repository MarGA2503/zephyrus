#!/usr/bin/env python
"""
Contains all auxiliar functions related with the function call method or
contract constructor method
"""
import copy
import math
import random
import sys
from ..auxiliar import cipherfunctions
from ..auxiliar import globalsettings

__author__ = "Mar Gimenez Aguilar"
__version__ = "1"
__maintainer__ = "Mar Gimenez Aguilar"
__email__ = "mar.gimeneza@gmail.com"

function_uint256 = 12
constructor_uint256 =  26

function_uint256_lens = {22:1020206}
function_uint256_zeros_lens = {22:{18:1}}

constructor_uint256_lens = {10:439}
constructor_uint256_zeros_lens = {10:{2:1}}

def getArgumentsfromhashes(hashes, data):
    """
    Returns the hashes of the function used and the data of them
    """
    arguments_used = []
    data_in_clear= []
    for x in data:
        function_hash = x[0:8]
        data_in_clear.append(x[8:])
        #print(function_hash)
        arguments_used.append(hashes[function_hash])
    #print(arguments_used)
    return arguments_used, data_in_clear

def constructorarguments(contractAbi):
    """
    Returns the arguments type of a contructor function
    """
    Find = False
    i = 0
    while not Find and i < len(contractAbi):
        dic = contractAbi[i]
        if dic['type'] == 'constructor':
            dic2 = dic['inputs']
            Find = True
        i += 1
    arguments = []
    for x in dic2:
        arguments.append( x['type'])
    if len(arguments) == 0:
        print("This constract has no constructor\n")
        exit(1)
    return arguments

def functionarguments(contractAbi, functionName):
    """
    Returns the arguments type for a contract or a determinated function
    """
    Find = False
    i = 0
    arguments = []
    if len(functionName) > 0:
        while not Find and i < len(contractAbi):
            dic = contractAbi[i]
            #print(dic['name'])
            if dic['name'] == functionName[0]:
                dic2 = dic['inputs']
                Find = True
            i += 1
        argsaux= []
        for x in dic2:
            argsaux.append( x['type'])
        arguments.append(argsaux)
        if len(arguments) == 0:
            print("This contract does not have that function name or the function does not have input arguments\n")
            exit(1)
    else:
        while i < len(contractAbi):
            dic = contractAbi[i]
            if 'inputs' in dic and dic['type'] != 'constructor':
                dic2 = dic['inputs']
                argsaux= []
                for x in dic2:
                    argsaux.append( x['type'])
            else:
                argsaux= []
            i += 1
            if len(argsaux) > 0:
                arguments.append(argsaux)
                if dic['type'] != 'constructor':
                    functionName.append(dic['name'])
        if len(arguments) == 0:
            print("This contract does not have functions or the functions do not have input arguments\n")
            exit(1)
    return arguments,functionName

def checklentotalmessage_old(arguments, maxsize = None):
    """
    Returns the length of each argument type
    """
    dynamic = []
    sizes = []
    final_arguments = []
    l = 0
    for i in range(len(arguments)):
        if arguments[i][-2:] == "[]":
            if arguments[i] == "bool[]":
                sizes.append(0)
            if maxsize!= None:
                sizes.append(maxsize)
            else:
                sizes.append(0)
                dynamic.append(l)
            l += 1
            final_arguments.append(arguments[i])
        elif  arguments[i][-1:] == "]":
            indexofopen = arguments[i].find("[")
            quantity = int( arguments[i][(indexofopen+1):-1])
            if arguments[i][0:4] == "uint":
                arg = arguments[i][0:indexofopen]
                tsize = int((int(arguments[i][4:indexofopen])/8)*2)
            elif arguments[i][0:3] == "int":
                arg = arguments[i][0:indexofopen]
                tsize = int((int(arguments[i][4:indexofopen])/8)*2)
            elif arguments[i][:indexofopen] == "address":
                arg = arguments[i][0:indexofopen]
                tsize = int((160/8)*2)
            elif arguments[i][:5] == "bytes":
                arg = arguments[i][0:indexofopen]
                tsize = int((int(arguments[i][5:indexofopen]))*2)
            elif arguments[i] == "function":
                arg = arguments[i][0:indexofopen]
                tsize = int((int(arguments[i][5:indexofopen]))*2)
            elif arguments[i] == "bool":
                arg = "bool"
                tsize = 0
            for z in range(quantity):
                sizes.append(tsize)
                final_arguments.append(arg)
                l += 1
        elif arguments[i][0:4] == "uint":
            sizes.append(int((int(arguments[i][4:])/8)*2))
            final_arguments.append(arguments[i])
            l += 1
        elif arguments[i][0:3] == "int":
            sizes.append(int((int(arguments[i][3:])/8)*2))
            final_arguments.append(arguments[i])
            l += 1
        elif arguments[i] == "address":
            sizes.append(int((160/8)*2))
            final_arguments.append(arguments[i])
            l += 1
        elif arguments[i] == "bytes":
            if maxsize!= None:
                sizes.append(maxsize)
            else:
                sizes.append(0)
                dynamic.append(l)
            l += 1
            final_arguments.append(arguments[i])
        elif arguments[i][:5] == "bytes":
            sizes.append(int((int(arguments[i][5:]))*2))
            final_arguments.append(arguments[i])
            l += 1
        elif arguments[i] == "function":
            arguments[i] = "bytes24"
            sizes.append(int((int(arguments[i][5:]))*2))
            final_arguments.append(arguments[i])
            l += 1
        elif arguments[i] == "string":
            if maxsize!= None:
                sizes.append(maxsize)
            else:
                sizes.append(0)
                dynamic.append(l)
            l += 1
            final_arguments.append(arguments[i])
        elif arguments[i] == "bool":
            sizes.append(0)
            final_arguments.append(arguments[i])
            l += 1
        else:
            print("Type not supported")
            exit(1)
    print(final_arguments)
    return sizes, dynamic, final_arguments

def checklentotalmessage(arguments, type, maxsize = None):
    """
    Returns the length of each argument type
    """
    dynamic = []
    sizes = []
    final_arguments = []
    l = 0
    # for i in range(len(arguments)):
    #     if arguments[i][-4:] == "[][]":
    #         arg = [arguments[i][:-2]]
    #         arguments = arguments[:i] + arg + arg + arguments[(i+1):]
    for i in range(len(arguments)):
        if arguments[i][-2:] == "[]" and arguments[i].count(']') == 1:
            # if arguments[i] == "bool[]":
            #     sizes.append(0)
            # if maxsize!= None:
            #     sizes.append(maxsize)
            # else:
            #     sizes.append(0)
            #     dynamic.append(l)
            # l += 1
            # final_arguments.append(arguments[i])
            print("Type not supported for now")
            exit(1)
        elif arguments[i].count('[]') == 2:
            # if arguments[i] == "bool[][]":
            #     sizes.append(0)
            # if maxsize!= None:
            #     sizes.append(maxsize)
            # else:
            #     sizes.append(0)
            #     dynamic.append(l)
            # l += 1
            # final_arguments.append(arguments[i])
            print("Type not supported for now")
            exit(1)
        elif arguments[i][-2:] == "[]" and arguments[i].count(']') == 2:
            # indices = [z for z, x in enumerate(arguments[i]) if x == "["]
            # indices2 = [z for z, x in enumerate(arguments[i]) if x == "]"]
            # quantity = int(arguments[i][(indices[0]+1):indices2[0]])
            # indexofopen = indices[0]
            # if arguments[i][0:4] == "uint":
            #     arg = arguments[i][0:indexofopen] + '[]'
            #     tsize = int((int(arguments[i][4:indexofopen])/8)*2)
            # elif arguments[i][0:3] == "int":
            #     arg = arguments[i][0:indexofopen] + '[]'
            #     tsize = int((int(arguments[i][4:indexofopen])/8)*2)
            # elif arguments[i][:indexofopen] == "address":
            #     arg = arguments[i][0:indexofopen] + '[]'
            #     tsize = int((160/8)*2)
            # elif arguments[i][:5] == "bytes":
            #     arg = arguments[i][0:indexofopen] + '[]'
            #     tsize = int((int(arguments[i][5:indexofopen]))*2)
            # elif arguments[i][:indexofopen] == "function":
            #     arg = arguments[i][0:indexofopen] + '[]'
            #     tsize = int((int(arguments[i][5:indexofopen]))*2)
            # elif arguments[i][:indexofopen] == "bool":
            #     arg = "bool" + '[]'
            #     tsize = 0
            # for z in range(quantity):
            #     sizes.append(tsize)
            #     final_arguments.append(arg)
            #     l += 1

            # if arguments[i] == "bool[][]":
            #     sizes.append(0)
            # if maxsize!= None:
            #     sizes.append(maxsize)
            # else:
            #     sizes.append(0)
            #     dynamic.append(l)
            # l += 1
            # final_arguments.append(arguments[i])
            print("Type not supported for now")
            exit(1)
        elif  arguments[i][-1:] == "]" and arguments[i].count(']') == 1:
            indexofopen = arguments[i].find("[")
            quantity = int( arguments[i][(indexofopen+1):-1])
            # if arguments[i][0:4] == "uint":
            #     arg = arguments[i][0:indexofopen]
            #     tsize = int((int(arguments[i][4:indexofopen])/8)*2)
            if arguments[i] == "uint256":
                arg = arguments[i][0:indexofopen]
                #tsize = int((int(arguments[i][4:indexofopen])/8)*2)
                if type == 'function':
                    tsize = function_uint256
                else:
                    tsize = constructor_uint256
            elif arguments[i][0:3] == "int":
                # arg = arguments[i][0:indexofopen]
                # tsize = int((int(arguments[i][4:indexofopen])/8)*2)
                print("Type not supported for now")
                exit(1)
            elif arguments[i][:indexofopen] == "address":
                arg = arguments[i][0:indexofopen]
                #tsize = int((160/8)*2)
                tsize = 160
            elif arguments[i] == "bytes32":
                arg = arguments[i][0:indexofopen]
                #tsize = int((int(arguments[i][5:indexofopen]))*2)
                if type == 'function':
                    tsize = 256
                else:
                    print("Type not supported for now")
                    exit(1)
            elif arguments[i][:indexofopen] == "function":
                # arg = arguments[i][0:indexofopen]
                # tsize = int((int(arguments[i][5:indexofopen]))*2)
                print("Type not supported for now")
                exit(1)
            elif arguments[i][:indexofopen] == "bool":
                # arg = "bool"
                # tsize = 0
                print("Type not supported for now")
                exit(1)
            else:
                print("Type not supported for now")
                exit(1)
            for z in range(quantity):
                sizes.append(tsize)
                final_arguments.append(arg)
                l += 1
        elif  arguments[i][-1:] == "]" and arguments[i].count(']') == 2 and not '[]' in arguments[i]:
            indices = [z for z, x in enumerate(arguments[i]) if x == "["]
            indices2 = [z for z, x in enumerate(arguments[i]) if x == "]"]
            num1 = int(arguments[i][(indices[0]+1):indices2[0]])
            num2 = int(arguments[i][(indices[1]+1):indices2[1]])
            quantity = num1 * num2
            indexofopen = indices[0]
            if arguments[i] == "uint256":
                arg = arguments[i][0:indexofopen]
                #tsize = int((int(arguments[i][4:indexofopen])/8)*2)
                if type == 'function':
                    tsize = function_uint256
                else:
                    tsize = constructor_uint256
            elif arguments[i][0:3] == "int":
                # arg = arguments[i][0:indexofopen]
                # tsize = int((int(arguments[i][4:indexofopen])/8)*2)
                print("Type not supported for now")
                exit(1)
            elif arguments[i][:indexofopen] == "address":
                arg = arguments[i][0:indexofopen]
                #tsize = int((160/8)*2)
                tsize = 160
            elif arguments[i]== "bytes32":
                arg = arguments[i][0:indexofopen]
                #tsize = int((int(arguments[i][5:indexofopen]))*2)
                if type == 'function':
                    tsize = 256
                else:
                    print("Type not supported for now")
                    exit(1)
            elif arguments[i][:indexofopen] == "function":
                # arg = arguments[i][0:indexofopen]
                # tsize = int((int(arguments[i][5:indexofopen]))*2)
                print("Type not supported for now")
                exit(1)
            elif arguments[i][:indexofopen] == "bool":
                # arg = "bool"
                # tsize = 0
                print("Type not supported for now")
                exit(1)
            else:
                print("Type not supported for now")
                exit(1)
            for z in range(quantity):
                sizes.append(tsize)
                final_arguments.append(arg)
                l += 1
        elif arguments[i]== "uint256":
            if type == 'function':
                tsize = function_uint256
            else:
                tsize = constructor_uint256
            sizes.append(tsize)
            #sizes.append(int((int(arguments[i][4:])/8)*2))
            final_arguments.append(arguments[i])
            l += 1
        elif arguments[i][0:3] == "int":
            # sizes.append(int((int(arguments[i][3:])/8)*2))
            # final_arguments.append(arguments[i])
            # l += 1
            print("Type not supported for now")
            exit(1)
        elif arguments[i] == "address":
            #sizes.append(int((160/8)*2))
            tsize = 160
            sizes.append(160)
            final_arguments.append(arguments[i])
            l += 1
        elif arguments[i] == "bytes":
            # if maxsize!= None:
            #     sizes.append(maxsize)
            # else:
            #     sizes.append(0)
            #     dynamic.append(l)
            # l += 1
            # final_arguments.append(arguments[i])
            print("Type not supported for now")
            exit(1)
        elif arguments[i]== "bytes32":
            #sizes.append(int((int(arguments[i][5:]))*2))
            if type == 'function':
                tsize = 256
            else:
                print("Type not supported for now")
                exit(1)
            sizes.append(tsize)
            final_arguments.append(arguments[i])
            l += 1
        elif arguments[i] == "function":
            # arguments[i] = "bytes24"
            # sizes.append(int((int(arguments[i][5:]))*2))
            # final_arguments.append(arguments[i])
            # l += 1
            print("Type not supported for now")
            exit(1)
        elif arguments[i] == "string":
            # if maxsize!= None:
            #     sizes.append(maxsize)
            # else:
            #     sizes.append(0)
            #     dynamic.append(l)
            # l += 1
            # final_arguments.append(arguments[i])
            print("Type not supported for now")
            exit(1)
        elif arguments[i] == "bool":
            # sizes.append(0)
            # final_arguments.append(arguments[i])
            # l += 1
            print("Type not supported for now")
            exit(1)
        else:
            print("Type not supported")
            exit(1)
    print(final_arguments)
    return sizes, dynamic, final_arguments

def checklentotalmessagecomplete_old(arguments, maxsize = None):
    """
    Returns the length of each argument type
    """
    dynamic = []
    sizes = []
    final_arguments = []
    l = 0
    # for i in range(len(arguments)):
    #     if arguments[i][-4:] == "[][]":
    #         arg = [arguments[i][:-2]]
    #         arguments = arguments[:i] + arg + arg + arguments[(i+1):]
    for i in range(len(arguments)):
        if arguments[i][-2:] == "[]" and arguments[i].count(']') == 1:
            if arguments[i] == "bool[]":
                sizes.append(0)
            if maxsize!= None:
                sizes.append(maxsize)
            else:
                sizes.append(0)
                dynamic.append(l)
            l += 1
            final_arguments.append(arguments[i])
        elif arguments[i].count('[]') == 2:
            if arguments[i] == "bool[][]":
                sizes.append(0)
            if maxsize!= None:
                sizes.append(maxsize)
            else:
                sizes.append(0)
                dynamic.append(l)
            l += 1
            final_arguments.append(arguments[i])
        elif arguments[i][-2:] == "[]" and arguments[i].count(']') == 2:
            # indices = [z for z, x in enumerate(arguments[i]) if x == "["]
            # indices2 = [z for z, x in enumerate(arguments[i]) if x == "]"]
            # quantity = int(arguments[i][(indices[0]+1):indices2[0]])
            # indexofopen = indices[0]
            # if arguments[i][0:4] == "uint":
            #     arg = arguments[i][0:indexofopen] + '[]'
            #     tsize = int((int(arguments[i][4:indexofopen])/8)*2)
            # elif arguments[i][0:3] == "int":
            #     arg = arguments[i][0:indexofopen] + '[]'
            #     tsize = int((int(arguments[i][4:indexofopen])/8)*2)
            # elif arguments[i][:indexofopen] == "address":
            #     arg = arguments[i][0:indexofopen] + '[]'
            #     tsize = int((160/8)*2)
            # elif arguments[i][:5] == "bytes":
            #     arg = arguments[i][0:indexofopen] + '[]'
            #     tsize = int((int(arguments[i][5:indexofopen]))*2)
            # elif arguments[i][:indexofopen] == "function":
            #     arg = arguments[i][0:indexofopen] + '[]'
            #     tsize = int((int(arguments[i][5:indexofopen]))*2)
            # elif arguments[i][:indexofopen] == "bool":
            #     arg = "bool" + '[]'
            #     tsize = 0
            # for z in range(quantity):
            #     sizes.append(tsize)
            #     final_arguments.append(arg)
            #     l += 1
            if arguments[i] == "bool[][]":
                sizes.append(0)
            if maxsize!= None:
                sizes.append(maxsize)
            else:
                sizes.append(0)
                dynamic.append(l)
            l += 1
            final_arguments.append(arguments[i])
        elif  arguments[i][-1:] == "]" and arguments[i].count(']') == 1:
            indexofopen = arguments[i].find("[")
            quantity = int( arguments[i][(indexofopen+1):-1])
            if arguments[i][0:4] == "uint":
                arg = arguments[i][0:indexofopen]
                tsize = int((int(arguments[i][4:indexofopen])/8)*2)
            elif arguments[i][0:3] == "int":
                arg = arguments[i][0:indexofopen]
                tsize = int((int(arguments[i][4:indexofopen])/8)*2)
            elif arguments[i][:indexofopen] == "address":
                arg = arguments[i][0:indexofopen]
                tsize = int((160/8)*2)
            elif arguments[i][:5] == "bytes":
                arg = arguments[i][0:indexofopen]
                tsize = int((int(arguments[i][5:indexofopen]))*2)
            elif arguments[i][:indexofopen] == "function":
                arg = arguments[i][0:indexofopen]
                tsize = int((int(arguments[i][5:indexofopen]))*2)
            elif arguments[i][:indexofopen] == "bool":
                arg = "bool"
                tsize = 0
            for z in range(quantity):
                sizes.append(tsize)
                final_arguments.append(arg)
                l += 1
        elif  arguments[i][-1:] == "]" and arguments[i].count(']') == 2 and not '[]' in arguments[i]:
            indices = [z for z, x in enumerate(arguments[i]) if x == "["]
            indices2 = [z for z, x in enumerate(arguments[i]) if x == "]"]
            num1 = int(arguments[i][(indices[0]+1):indices2[0]])
            num2 = int(arguments[i][(indices[1]+1):indices2[1]])
            quantity = num1 * num2
            indexofopen = indices[0]
            if arguments[i][0:4] == "uint":
                arg = arguments[i][0:indexofopen]
                tsize = int((int(arguments[i][4:indexofopen])/8)*2)
            elif arguments[i][0:3] == "int":
                arg = arguments[i][0:indexofopen]
                tsize = int((int(arguments[i][4:indexofopen])/8)*2)
            elif arguments[i][:indexofopen] == "address":
                arg = arguments[i][0:indexofopen]
                tsize = int((160/8)*2)
            elif arguments[i][:5] == "bytes":
                arg = arguments[i][0:indexofopen]
                tsize = int((int(arguments[i][5:indexofopen]))*2)
            elif arguments[i][:indexofopen] == "function":
                arg = arguments[i][0:indexofopen]
                tsize = int((int(arguments[i][5:indexofopen]))*2)
            elif arguments[i][:indexofopen] == "bool":
                arg = "bool"
                tsize = 0
            for z in range(quantity):
                sizes.append(tsize)
                final_arguments.append(arg)
                l += 1
        elif arguments[i][0:4] == "uint":
            sizes.append(int((int(arguments[i][4:])/8)*2))
            final_arguments.append(arguments[i])
            l += 1
        elif arguments[i][0:3] == "int":
            sizes.append(int((int(arguments[i][3:])/8)*2))
            final_arguments.append(arguments[i])
            l += 1
        elif arguments[i] == "address":
            sizes.append(int((160/8)*2))
            final_arguments.append(arguments[i])
            l += 1
        elif arguments[i] == "bytes":
            if maxsize!= None:
                sizes.append(maxsize)
            else:
                sizes.append(0)
                dynamic.append(l)
            l += 1
            final_arguments.append(arguments[i])
        elif arguments[i][:5] == "bytes":
            sizes.append(int((int(arguments[i][5:]))*2))
            final_arguments.append(arguments[i])
            l += 1
        elif arguments[i] == "function":
            arguments[i] = "bytes24"
            sizes.append(int((int(arguments[i][5:]))*2))
            final_arguments.append(arguments[i])
            l += 1
        elif arguments[i] == "string":
            if maxsize!= None:
                sizes.append(maxsize)
            else:
                sizes.append(0)
                dynamic.append(l)
            l += 1
            final_arguments.append(arguments[i])
        elif arguments[i] == "bool":
            sizes.append(0)
            final_arguments.append(arguments[i])
            l += 1
        else:
            print("Type not supported")
            exit(1)
    print(final_arguments)
    return sizes, dynamic, final_arguments
#TODO message will be divided in parts by main, calling the function the necesary times

def reajustf (sizes, sizesm, arguments = []):
    """
    Divides the weight of the message between arguments making sure it is not bigger
    than the maximun type length
    """
    sizesc = copy.deepcopy(sizes)
    sizesmc = copy.deepcopy(sizesm)
    result = []
    rest = 0
    done = False
    args = arguments
    while not done:
        sizesmcc = copy.deepcopy(sizesmc)
        for i in range(len(sizesc)):
            sizesmc[i] += rest
            if len(arguments)>0 and (arguments[i] == "bool[]" or arguments[i] == "bool"):
                #print(arguments[i], rest)
                rest = sizesmc[i]
                sizesmc[i] = 0
            elif sizesc[i] == 0 :
                rest = 0
            elif sizesmc[i] >= sizesc[i]:
                #print("rest",rest,sizesmc[i])
                rest = sizesmc[i] - sizesc[i]
                #print("rest",rest)
                sizesmc[i] = sizesc[i]
            else:
                rest = 0
            #print("s",sizesmc[i])
        if sizesmcc == sizesmc:
            done= True
    #print("return", sizesmc,sizesmcc)
    return sizesmc
def makeeven (arguments, sizesm):
    """
    Divides the message so each argument has an even number of characters
    """
    #for extracting the message, elements must be pairs
    #print(arguments, sizesm)
    allEven = False
    rest = 0
    while not allEven:
        sizesmcc = copy.deepcopy(sizesm)
        for i in range(len(arguments)):
            #print(arguments[i])
            if rest != 0 and (arguments[i] != "bool[]" or arguments[i] != "bool"):
                sizesm[i] += 1
                rest = 0
            if sizesm[i] % 2 != 0 and (arguments[i] != "bool[]" or arguments[i] != "bool"):
                sizesm[i] -= 1
                rest = 1
        if sizesmcc == sizesm:
            allEven= True
    return  sizesm


#TODO assure minimun in each case for reajust
def dividemessage (arguments, message, sizes, dynamic, reajust = True):
    """
    Divides the message between the arguments taking into account if the user
    wants the weight to be divided between them
    """
    if len(dynamic) == 0:
        #print("entro aqui")
        if reajust:
            c, r = divmod (len(message),len(arguments))
            #print(c,r,len(message) )
            sizesm = [c] * len(arguments)
            #The we add the rest to the biggest element
            #print(sizes)
            element, index = max([(v,i) for i,v in enumerate(sizes)])
            #print("h", sizesm,arguments)
            sizesm[index] += r
            #print("divide",element, index,sizesm,c,r)
            sizesmn = reajustf(sizes, sizesm, arguments)
            #print("divide",element, index,sizesmn,c,r)
        else:
            #ot the real size per se, wiil be selecting these number until message empty
            sizesmn = copy.deepcopy(sizes)
    else:
        if reajust:
            c, r = divmod (len(message),len(arguments))
            sizesm = [c] * len(arguments)
            #The we add the rest to the last dynamic element
            #element, index = max([(v,i) for i,v in enumerate(sizes)])
            sizesm[dynamic[-1]] += r
            #print("divide", index,sizesm,c,r,dynamic)
            #sizesm[index] += r
            #print("divide",sizesm,c,r)
            sizesmn = reajustf(sizes, sizesm, arguments)
            #print("divide",sizesm,c,r)
            #Divide weight between dynamics
            total = 0
            for i in range(len(sizes)):
                if sizes[i] == 0:
                    total+= sizesmn[i]
            perdelement, rest = divmod (total,len(dynamic))
            for i in range(len(sizes)):
                if sizes[i] == 0:
                    sizesmn[i] = perdelement
            sizesmn[dynamic[0]] += rest
            #print(sizesmn)
        else:
            #Get the total amount , fill the spaces and the rest to the dynamic
            sizesmn = copy.deepcopy(sizes)
            #print(sizes)
            sumnod = sum(sizes)
            restmen = len(message) - sumnod
            if restmen > 0:
                perdelement, rest = divmod (restmen,len(dynamic))
                for i in range(len(sizes)):
                    if sizes[i] == 0:
                        sizesmn[i] = perdelement
                sizesmn[dynamic[0]] += rest
    return makeeven (arguments, sizesmn)

def checkleninfirst(sizes,sizesn, lens):
    """
    Retrieves the information necessary to extract the message correctly
    """
    newsize = copy.deepcopy(sizesn)
    if lens > sizesn[0] and sizes[0] >= lens:
        rest = lens - sizesn[0]
        newsize[0] += rest
        it = int(rest/2)
        x = 1
        while it > 0:
            sizesrest = copy.deepcopy(newsize[1:])
            element, index = max([(v,i) for i,v in enumerate(sizesrest)])
            newsize[index] -= 2
            it -= 1
    elif lens > sizesn[0] and sizes[0] < lens:
        found = False
        i = 0
        cont = 0
        while i < (len(sizesn)) and not Found:
            cont += sizes[i]
            rest = cont - lens
            if rest >= 0:
                Found = True
            else:
                i += 1
        x = 0
        while i >= 0:
            rest = lens - sizesn[x]
            newsize[x] += rest
            it = int(rest/2)
            while it > 0 and x < (len(newsize)-1):
                sizesrest = copy.deepcopy(newsize[(x+1):])
                element, index = max([(v,z) for z,v in enumerate(sizesrest)])
                newsize[index] -= 2
                it -= 1
            x += 1
    return newsize


            # if x%(len(newsize)) != 0:
            #     if newsize[x%(len(newsize))] >=2:
            #         newsize[x%(len(newsize))] -= 2

#TODO change if eliminate arguments
# def createcall (arguments, message, sizesmn, dynamic, reajust = True):
#     call = ""
#     aux = ""
#     auxmessage = copy.deepcopy(message)
#     if len(dynamic) == 0:
#         print("aqui")
#         for i in range(len(arguments)):
#             #Padding a la derecha
#             if arguments[i][:5]== "bytes":
#                 call += auxmessage[:sizesmn[i]]
#                 zeros = 64 - len(auxmessage[:sizesmn[i]])
#                 call += ("0" * zeros)
#                 auxmessage = auxmessage[sizesmn[i]:]
#             else:
#                 print("heyy")
#                 zeros = 64 - len(auxmessage[:sizesmn[i]])
#                 call += ("0" * zeros)
#                 auxm = auxmessage[:sizesmn[i]]
#                 x = len(auxm) - 1
#                 while x >= 0:
#                     call += auxm[x]
#                     x -= 1
#                 auxmessage = auxmessage[sizesmn[i]:]
#     else:
#         cont = ""
#         for i in range(len(arguments)):
#             if i in dynamic:
#                 #Speial case--> array
#                 #Check for the len and divide
#                 if arguments[i][-2:] == "[]":
#                     lenelement = checklentotalmessage([arguments[i][:-2]])
#                     #how many...
#                     print(lenelement)
#                     numberofelements = math.ceil(int(sizesmn[i])/(lenelement)[0][0])
#                     print("numero", numberofelements)
#                     sizesele = [lenelement[0][0]] * numberofelements
#                     print(sizesele)
#                     argumentsnew = [arguments[i][:-2]] * numberofelements
#                     print(argumentsnew)
#                     sizesnew = dividemessage (argumentsnew, auxmessage[:sizesmn[i]], sizesele ,[], reajust)
#                     aux2 = ""
#                     numberelementinhex = hex(numberofelements)[2:]
#                     zeros = 64 - len(numberelementinhex)
#                     part1 = ("0" * zeros)
#                     aux2 += (part1 + numberelementinhex)
#                     print(argumentsnew)
#                     for x in range(len(argumentsnew)):
#                         print(x, aux2)
#                         zeros = 64 - sizesnew[x]
#                         print(sizesnew,zeros)
#                         part1 = ("0" * zeros)
#                         print(part1)
#                         auxm = auxmessage[:sizesnew[x]]
#                         part2 = ""
#                         z = len(auxm) - 1
#                         while z >= 0:
#                             part2 += auxm[z]
#                             z -= 1
#                         print(part2)
#                         auxmessage = auxmessage[sizesnew[x]:]
#                         aux2 += (part1 + part2)
#                         print(aux2)
#                     position = int(len(cont)/2)
#                     position += (len(arguments)- i)* 32
#                     positionhex = hex(position)[2:]
#                     zeros = 64 - len(positionhex)
#                     part1 = ("0" * zeros)
#                     cont = cont + part1 + positionhex + aux2
#                     call = call + part1 + positionhex
#                     print("c",call)
#                     aux += aux2
#                 else:
#                     #Now bytes and strings.. padding at right
#                     #can be the lon we want so we have to view module to see remaining
#                     #use divmod
#                     #TODO: the len of the message cant be bigger than f * 64
#                     aux2 = ""
#                     leninhex = hex(int(sizesmn[i]/2))[2:]
#                     zeros = 64 - len(leninhex)
#                     print("zeros", zeros)
#                     part1 = ("0" * zeros)
#                     aux2 += (part1 + leninhex)
#                     print("aux", aux2)
#                     c, r = divmod (sizesmn[i],64)
#                     zeros = 64 - r
#                     part1 = auxmessage[:sizesmn[i]]
#                     auxmessage = auxmessage[sizesmn[i]:]
#                     part2 = ("0" * zeros)
#                     aux2 += (part1 + part2)
#                     position = int(len(call)/2 )
#                     position += (len(arguments)- i)* 32
#                     positionhex = hex(position)[2:]
#                     zeros = 64 - len(positionhex)
#                     part1 = ("0" * zeros)
#                     cont = cont + part1 + positionhex + aux2
#                     call = call + part1 + positionhex
#                     print(call)
#                     aux += aux2
#
#             else:
#                 if arguments[i][:5]== "bytes":
#                     part1 = auxmessage[:sizesmn[i]]
#                     auxmessage = auxmessage[sizesmn[i]:]
#                     zeros = 64 - len(part1)
#                     part2 = ("0" * zeros)
#                     call += (part1 + part2)
#                     cont += (part1 + part2)
#                 else:
#                     part2 = auxmessage[:sizesmn[i]]
#                     zeros = 64 - len(part2)
#                     part1 = ("0" * zeros)
#                     auxm = auxmessage[:sizesmn[i]]
#                     x = len(auxm) - 1
#                     part2 = ""
#                     while x >= 0:
#                         part2 += auxm[x]
#                         x -= 1
#                     auxmessage = auxmessage[sizesmn[i]:]
#                     call += (part1 + part2)
#                     print(call)
#                     cont += (part1 + part2)
#
#     return call + aux
def readcall_old (arguments, code, sizes, dynamic,ChaCha20key):
    """
    Retrieves the message from a function call
    """
    call = ""
    aux = ""
    auxcode = copy.deepcopy(code)
    message = ""
    cont = ""
    to_read = 0
    rest = 0
    data = ""
    form = ""
    dynamics_sizes = []
    i = 0
    #First we see the lenght of the message in the data and how it distributed:
    while arguments[i] == "bool[]" or arguments[i] == "bool":
        #There is no information here, we have to pass to the next to get to the actual message
        i += 1
    if arguments[i][-2:] == "[]":
        #array_start = int(code[0:64], 16) * 2
        array_start = int(code[(i*64):(i*64 + 64)], 16) * 2
        lenelement = checklentotalmessage([arguments[i][:-2]])
        #how many...
        #print(lenelement)
        numberofelements = int(code[array_start:(array_start + 64)], 16)
        #The first element must contain all the important info if the size
        #is not less than this. We start reading the length of the code lenght
        index_to_first_element= (array_start + 64 + 64)
        #integers so we read backwards
        if ChaCha20key != "":
            length_of_len = (code[(index_to_first_element-2):index_to_first_element][::-1])
            length_of_len = int(cipherfunctions.ecipherChaCha20(length_of_len, ChaCha20key, "hex"),16)
        else:
            length_of_len = int(code[(index_to_first_element-2):index_to_first_element][::-1],16)
        if lenelement[0][0] >= (2+ length_of_len +2):
            index_to_length = (index_to_first_element-2)
            if ChaCha20key != "":
                length_data = (code[(index_to_length-length_of_len):index_to_length][::-1])
                form = (code[(index_to_length-length_of_len -2):(index_to_length-length_of_len)][::-1])
                lengthform = cipherfunctions.ecipherChaCha20(length_data + form, ChaCha20key, "hex")
                length_data = int(lengthform[:-2],16)
                form = lengthform[-2:]
            else:
            #if ChaCha20 extract the two at the same time or descipher the two
                length_data = int(code[(index_to_length-length_of_len):index_to_length][::-1],16)
                form = (code[(index_to_length-length_of_len -2):(index_to_length-length_of_len)][::-1])
        elif numberofelements > 1:
            to_read = lenelement[0][0] - 2
            index_to_length = (index_to_first_element-2)
            data = code[(index_to_length-to_read):index_to_length]
            rest = (length_of_len + 2) - to_read
            index_to_length = (index_to_first_element)
            c = 1
            while rest != 0 and c < numberofelements:
                if rest > lenelement[0][0]:
                    to_read = lenelement[0][0]
                else:
                    to_read = rest
                index_to_length = (index_to_length + 64)
                data = code[(index_to_length-to_read):index_to_length]
                rest = rest - to_read
                c += 1
            if rest == 0:
                if ChaCha20key != "":
                    length_data = (data[:-2][::-1])
                    form = data[-2:][::-1]
                    lengthform = cipherfunctions.ecipherChaCha20(length_data + form, ChaCha20key, "hex")
                    length_data = int(lengthform[:-2],16)
                    form = lengthform[-2:]
                else:
                    length_data = int(data[:-2][::-1],16)
                    form = data[-2:][::-1]
        else:
            to_read = lenelement[0][0] - 2
            index_to_length = (index_to_first_element-2)
            data = code[(index_to_length-to_read):index_to_length]
            rest = (length_of_len + 2) - to_read

    elif arguments[i] == "string" or arguments[i] == "bytes":
        #array_start = int(code[0:64], 16) * 2
        #print("entro")
        array_start = int(code[(i*64):(i*64 + 64)], 16) * 2
        numberofelements = int(code[array_start:(array_start + 64)], 16) * 2
        index_to_first_element= (array_start + 64)
        if ChaCha20key != "":
            length_of_len = (code[index_to_first_element:(index_to_first_element+2)])
            length_of_len = int(cipherfunctions.ecipherChaCha20(length_of_len, ChaCha20key, "hex"),16)
        else:
            length_of_len = int(code[index_to_first_element:(index_to_first_element+2)],16)
        #we read in order
        #print(numberofelements >= (2+ length_of_len +2))
        if numberofelements >= (2+ length_of_len +2):
            index_to_length = (index_to_first_element+2)
            if ChaCha20key != "":
                length_data = (code[index_to_length:(index_to_length+length_of_len)])
                form = code[(index_to_length+length_of_len):(index_to_length+length_of_len+2)]
                lengthform = cipherfunctions.ecipherChaCha20(length_data + form, ChaCha20key, "hex")
                length_data = int(lengthform[:-2],16)
                form = lengthform[-2:]
            else:
                length_data = int(code[index_to_length:(index_to_length+length_of_len)],16)
                form = code[(index_to_length+length_of_len):(index_to_length+length_of_len+2)]
        else:
            to_read = numberofelements - 2
            index_to_length = (index_to_first_element+2)
            data = code[index_to_length:(index_to_length+to_read)]
            rest = (length_of_len + 2) - to_read
    else:
        if arguments[i][:5]== "bytes":
            if ChaCha20key != "":
                #length_of_len = (code[:2])
                aux = code[(i*64):(i*64 + 64)]
                length_of_len = (aux[:2])
                length_of_len = int(cipherfunctions.ecipherChaCha20(length_of_len, ChaCha20key, "hex"),16)
            else:
                aux = code[(i*64):(i*64 + 64)]
                length_of_len = int(aux[:2],16)
            if sizes[i] >= (2+ length_of_len +2):
                if ChaCha20key != "":
                    #length_data = (code[2:(2+length_of_len)])
                    length_data = (aux[2:(2+length_of_len)])
                    #form = code[(2+length_of_len):(2+length_of_len +2)]
                    form = aux[(2+length_of_len):(2+length_of_len +2)]
                    lengthform = cipherfunctions.ecipherChaCha20(length_data + form, ChaCha20key, "hex")
                    length_data = int(lengthform[:-2],16)
                    form = lengthform[-2:]
                else:
                    # length_data = int(code[2:(2+length_of_len)],16)
                    # form = code[(2+length_of_len):(2+length_of_len +2)]
                    length_data = int(aux[2:(2+length_of_len)],16)
                    form = aux[(2+length_of_len):(2+length_of_len +2)]

            else:
                to_read = sizes[i] - 2
                index_to_length = ((i*64)+2)
                data = code[index_to_length:(index_to_length+to_read)]
                rest = (length_of_len + 2) - to_read
        else:
            #print(code[62:64])
            #print(code,ChaCha20key, cipherfunctions.nonce)
            if ChaCha20key != "":
                aux = code[(i*64):(i*64 + 64)]
                length_of_len = (aux[62:64][::-1])
                #length_of_len = (code[62:64][::-1])
                #print("l",length_of_len,rest)
                length_of_len = int(cipherfunctions.ecipherChaCha20(length_of_len, ChaCha20key, "hex"),16)
                #print("l",length_of_len,rest)
            else:
                aux = code[(i*64):(i*64 + 64)]
                #length_of_len = int(code[62:64][::-1],16)
                length_of_len = int(aux[62:64][::-1],16)
            #print(length_of_len)
            #print(sizes)
            if sizes[i] >= (2+ length_of_len +2):
                if ChaCha20key != "":
                    #length_data = (code[(62-length_of_len):62][::-1])
                    #form = code[(62-length_of_len-2):(62-length_of_len)][::-1]
                    length_data = (aux[(62-length_of_len):62][::-1])
                    form = aux[(62-length_of_len-2):(62-length_of_len)][::-1]
                    lengthform = cipherfunctions.ecipherChaCha20(length_data + form, ChaCha20key, "hex")
                    length_data = int(lengthform[:-2],16)
                    form = lengthform[-2:]
                    #print("Lengthdadat", form, length_data,lengthform)
                else:
                    # length_data = int(code[(62-length_of_len):62][::-1],16)
                    # form = code[(62-length_of_len-2):(62-length_of_len)][::-1]

                    length_data = int(aux[(62-length_of_len):62][::-1],16)
                    form = aux[(62-length_of_len-2):(62-length_of_len)][::-1]
            else:
                to_read = sizes[i] - 2
                #index_to_length = (62)
                index_to_length = (i*64 + 64) -2
                data = code[(index_to_length-to_read):index_to_length]
                rest = (length_of_len + 2) - to_read

    #TODO: case uin8 or very small argument
    if rest != 0:
        print("TODO\n")
        exit(1)
    if form[1]== "1":
        reajust = True
    else:
        reajust = False
    #fake_message = 'f' * len(hex(globalsettings.max_number_transactions)[2:]) * 2 + length_of_len * 'f' + 'f'*length_data
    #This 2 is what we have to read to know how much the length occupies
    fake_message = 2*'f'+ length_of_len * 'f' + 'f'*length_data
    i = 0
    #print(fake_message,form,length_data,reajust,dynamic,sizes)
    if len(dynamic) == 0:
        #print("este",arguments, sizes)
        sizesn = dividemessage (arguments, fake_message, sizes, dynamic, reajust)
    else:
        #Check the length of the dynamics if
        for i in dynamic:
            if arguments[i][-2:] == "[]":
                array_start = int(code[(i*64):(i*64 + 64)], 16) * 2
                lenelement = checklentotalmessage([arguments[i][:-2]])
                #how many...
                #print(lenelement)
                numberofelements = int(code[array_start:(array_start + 64)], 16)
                sizes[i] = numberofelements * lenelement[0][0]
            elif arguments[i] == "string" or arguments[i] == "bytes":
                array_start = int(code[(i*64):(i*64 + 64)], 16) * 2
                numberofelements = int(code[array_start:(array_start + 64)], 16) * 2
                c, r = divmod(numberofelements,64)
                if r != 0:
                    c += 1
                sizes[i] = c * 64
                #print(numberofelements,code[array_start:(array_start + 64)],divmod(numberofelements,64),sizes[i])
        #print("aquel", arguments, sizes)
        sizesn = dividemessage (arguments, fake_message, sizes, [], reajust)
        #print("sigo", sizesn)
    #print(sizesn)
    #Now I can read. Two cases, reajust and not
    lengthsmin = 2 + length_of_len + len(form)
    sizesnn = checkleninfirst(sizes,sizesn, lengthsmin)
    #print(sizesnn)
    sizes = copy.deepcopy(sizesnn)
    if reajust:
        message = ""
        for i in range(len(sizesn)):
            if arguments[i] == "bool[]" or arguments[i] == "bool":
                continue
            if arguments[i][-2:] == "[]":
                array_start = int(code[(i*64):(i*64 + 64)], 16) * 2
                numberofelements = int(code[array_start:(array_start + 64)], 16)
                to_read = copy.deepcopy(sizes[i])
                #print(to_read,sizes)
                index_element= (array_start + 64 + 64)
                lenelement = checklentotalmessage([arguments[i][:-2]])
                for x in range(numberofelements):
                    if to_read >= lenelement[0][0]:
                        aux =  code[(index_element-lenelement[0][0]):index_element][::-1]
                        message += aux
                        index_element += 64
                        to_read -= lenelement[0][0]
                    else:
                        aux =  code[(index_element-to_read):index_element][::-1]
                        #print(aux)
                        message += aux

            elif arguments[i] == "string" or arguments[i] == "bytes":
                array_start = int(code[(i*64):(i*64 + 64)], 16) * 2
                #print("entro2")
                #print(code[(i*64):(i*64 + 64)], code[array_start:(array_start + 64)], code[(array_start+64):(array_start+64+numberofelements)])
                #print("h",code[i*64:])
                #print("u",code)
                numberofelements = int(code[array_start:(array_start + 64)], 16) * 2
                aux = code[(array_start+64):(array_start+64+numberofelements)]
                #print(numberofelements, aux, code, array_start )
                #print(aux)
                message += aux
            else:
                if arguments[i][:5]== "bytes":
                    aux = code[(i*64):(i*64 + 64)]
                    aux= aux[:sizes[i]]
                    message += aux
                else:
                    aux = code[(i*64):(i*64 + 64)]
                    aux= aux[-sizes[i]:][::-1]
                    #print("aquí", code[(i*64):(i*64 + 64)], sizes[i], aux,len(aux))
                    message += aux
    else:
        #print("por aqui")
        i = 0
        to_read_total = 2  + length_of_len + length_data
        #print(to_read_total)
        while i < len(sizesn) and to_read_total > 0:
            if arguments[i] == "bool[]" or arguments[i] == "bool":
                pass
            elif arguments[i][-2:] == "[]":
                array_start = int(code[(i*64):(i*64 + 64)], 16) * 2
                numberofelements = int(code[array_start:(array_start + 64)], 16)
                to_read = copy.deepcopy(sizes[i])
                index_element= (array_start + 64 + 64)
                lenelement = checklentotalmessage([arguments[i][:-2]])
                x = 0
                while x < (numberofelements) and to_read_total != 0:
                    if to_read_total >= lenelement[0][0]:
                        aux =  code[(index_element-lenelement[0][0]):index_element][::-1]
                        message += aux
                        index_element += 64
                        to_read_total -= lenelement[0][0]
                    else:
                        aux =  code[(index_element-to_read_total):index_element][::-1]
                        message += aux
                        to_read_total -= to_read_total

            elif arguments[i] == "string" or arguments[0] == "bytes":
                array_start = int(code[(i*64):(i*64 + 64)], 16) * 2
                numberofelements = int(code[array_start:(array_start + 64)], 16) * 2
                aux = code[(array_start+64):(array_start+64+numberofelements)]
                message += aux
                to_read_total -= numberofelements
            else:
                if arguments[i][:5]== "bytes":
                    aux = code[(i*64):(i*64 + 64)]
                    if to_read_total <= sizes[i] :
                        aux = aux[:to_read_total]
                        to_read_total -= to_read_total
                    else:
                        aux= aux[:sizes[i]]
                        to_read_total -= sizes[i]

                    message += aux
                else:
                    aux = code[(i*64):(i*64 + 64)]
                    #print("aux", aux)
                    if to_read_total <= sizes[i] :
                        aux= aux[-to_read_total:][::-1]
                        #print("aux", aux)
                        to_read_total -= to_read_total
                    else:
                        aux= aux[-sizes[i]:][::-1]
                        #print("aux", aux)
                        to_read_total -= sizes[i]
                    message += aux
            i+=1
    #print(message)
    message = message[(2 + 2 + length_of_len):]
    #print(message)
    return message


def readcall (arguments, code, sizes, dynamic,dict_valuespossible, ChaCha20key):
    """
    Retrieves the message from a function call
    """
    call = ""
    aux = ""
    auxcode = copy.deepcopy(code)
    message = ""
    cont = ""
    to_read = 0
    rest = 0
    data = ""
    form = ""
    dynamics_sizes = []
    i = 0
    #First we see the lenght of the message in the data and how it distributed:
    # while arguments[i] == "bool[]" or arguments[i] == "bool":
    #     #There is no information here, we have to pass to the next to get to the actual message
    #     i += 1
    # if arguments[i][-2:] == "[]":
    #     #array_start = int(code[0:64], 16) * 2
    #     array_start = int(code[(i*64):(i*64 + 64)], 16) * 2
    #     lenelement = checklentotalmessage([arguments[i][:-2]])
    #     #how many...
    #     #print(lenelement)
    #     numberofelements = int(code[array_start:(array_start + 64)], 16)
    #     #The first element must contain all the important info if the size
    #     #is not less than this. We start reading the length of the code lenght
    #     index_to_first_element= (array_start + 64 + 64)
    #     #integers so we read backwards
    #     if ChaCha20key != "":
    #         length_of_len = (code[(index_to_first_element-2):index_to_first_element][::-1])
    #         length_of_len = int(cipherfunctions.ecipherChaCha20(length_of_len, ChaCha20key, "hex"),16)
    #     else:
    #         length_of_len = int(code[(index_to_first_element-2):index_to_first_element][::-1],16)
    #     if lenelement[0][0] >= (2+ length_of_len +2):
    #         index_to_length = (index_to_first_element-2)
    #         if ChaCha20key != "":
    #             length_data = (code[(index_to_length-length_of_len):index_to_length][::-1])
    #             form = (code[(index_to_length-length_of_len -2):(index_to_length-length_of_len)][::-1])
    #             lengthform = cipherfunctions.ecipherChaCha20(length_data + form, ChaCha20key, "hex")
    #             length_data = int(lengthform[:-2],16)
    #             form = lengthform[-2:]
    #         else:
    #         #if ChaCha20 extract the two at the same time or descipher the two
    #             length_data = int(code[(index_to_length-length_of_len):index_to_length][::-1],16)
    #             form = (code[(index_to_length-length_of_len -2):(index_to_length-length_of_len)][::-1])
    #     elif numberofelements > 1:
    #         to_read = lenelement[0][0] - 2
    #         index_to_length = (index_to_first_element-2)
    #         data = code[(index_to_length-to_read):index_to_length]
    #         rest = (length_of_len + 2) - to_read
    #         index_to_length = (index_to_first_element)
    #         c = 1
    #         while rest != 0 and c < numberofelements:
    #             if rest > lenelement[0][0]:
    #                 to_read = lenelement[0][0]
    #             else:
    #                 to_read = rest
    #             index_to_length = (index_to_length + 64)
    #             data = code[(index_to_length-to_read):index_to_length]
    #             rest = rest - to_read
    #             c += 1
    #         if rest == 0:
    #             if ChaCha20key != "":
    #                 length_data = (data[:-2][::-1])
    #                 form = data[-2:][::-1]
    #                 lengthform = cipherfunctions.ecipherChaCha20(length_data + form, ChaCha20key, "hex")
    #                 length_data = int(lengthform[:-2],16)
    #                 form = lengthform[-2:]
    #             else:
    #                 length_data = int(data[:-2][::-1],16)
    #                 form = data[-2:][::-1]
    #     else:
    #         to_read = lenelement[0][0] - 2
    #         index_to_length = (index_to_first_element-2)
    #         data = code[(index_to_length-to_read):index_to_length]
    #         rest = (length_of_len + 2) - to_read
    #
    # elif arguments[i] == "string" or arguments[i] == "bytes":
    #     #array_start = int(code[0:64], 16) * 2
    #     #print("entro")
    #     array_start = int(code[(i*64):(i*64 + 64)], 16) * 2
    #     numberofelements = int(code[array_start:(array_start + 64)], 16) * 2
    #     index_to_first_element= (array_start + 64)
    #     if ChaCha20key != "":
    #         length_of_len = (code[index_to_first_element:(index_to_first_element+2)])
    #         length_of_len = int(cipherfunctions.ecipherChaCha20(length_of_len, ChaCha20key, "hex"),16)
    #     else:
    #         length_of_len = int(code[index_to_first_element:(index_to_first_element+2)],16)
    #     #we read in order
    #     #print(numberofelements >= (2+ length_of_len +2))
    #     if numberofelements >= (2+ length_of_len +2):
    #         index_to_length = (index_to_first_element+2)
    #         if ChaCha20key != "":
    #             length_data = (code[index_to_length:(index_to_length+length_of_len)])
    #             form = code[(index_to_length+length_of_len):(index_to_length+length_of_len+2)]
    #             lengthform = cipherfunctions.ecipherChaCha20(length_data + form, ChaCha20key, "hex")
    #             length_data = int(lengthform[:-2],16)
    #             form = lengthform[-2:]
    #         else:
    #             length_data = int(code[index_to_length:(index_to_length+length_of_len)],16)
    #             form = code[(index_to_length+length_of_len):(index_to_length+length_of_len+2)]
    #     else:
    #         to_read = numberofelements - 2
    #         index_to_length = (index_to_first_element+2)
    #         data = code[index_to_length:(index_to_length+to_read)]
    #         rest = (length_of_len + 2) - to_read
    # else:
    #     if arguments[i][:5]== "bytes":
    #         aux = code[(i*64):(i*64 + 64)]
    #
    #         if ChaCha20key != "":
    #             #length_of_len = (code[:2])
    #             aux = code[(i*64):(i*64 + 64)]
    #             length_of_len = (aux[:2])
    #             length_of_len = int(cipherfunctions.ecipherChaCha20(length_of_len, ChaCha20key, "hex"),16)
    #         else:
    #             aux = code[(i*64):(i*64 + 64)]
    #             length_of_len = int(aux[:2],16)
    #         if sizes[i] >= (2+ length_of_len +2):
    #             if ChaCha20key != "":
    #                 #length_data = (code[2:(2+length_of_len)])
    #                 length_data = (aux[2:(2+length_of_len)])
    #                 #form = code[(2+length_of_len):(2+length_of_len +2)]
    #                 form = aux[(2+length_of_len):(2+length_of_len +2)]
    #                 lengthform = cipherfunctions.ecipherChaCha20(length_data + form, ChaCha20key, "hex")
    #                 length_data = int(lengthform[:-2],16)
    #                 form = lengthform[-2:]
    #             else:
    #                 # length_data = int(code[2:(2+length_of_len)],16)
    #                 # form = code[(2+length_of_len):(2+length_of_len +2)]
    #                 length_data = int(aux[2:(2+length_of_len)],16)
    #                 form = aux[(2+length_of_len):(2+length_of_len +2)]
    #
    #         else:
    #             to_read = sizes[i] - 2
    #             index_to_length = ((i*64)+2)
    #             data = code[index_to_length:(index_to_length+to_read)]
    #             rest = (length_of_len + 2) - to_read
    #     else:
    #         #print(code[62:64])
    #         #print(code,ChaCha20key, cipherfunctions.nonce)
    #         if ChaCha20key != "":
    #             aux = code[(i*64):(i*64 + 64)]
    #             length_of_len = (aux[62:64][::-1])
    #             #length_of_len = (code[62:64][::-1])
    #             #print("l",length_of_len,rest)
    #             length_of_len = int(cipherfunctions.ecipherChaCha20(length_of_len, ChaCha20key, "hex"),16)
    #             #print("l",length_of_len,rest)
    #         else:
    #             aux = code[(i*64):(i*64 + 64)]
    #             #length_of_len = int(code[62:64][::-1],16)
    #             length_of_len = int(aux[62:64][::-1],16)
    #         #print(length_of_len)
    #         #print(sizes)
    #         if sizes[i] >= (2+ length_of_len +2):
    #             if ChaCha20key != "":
    #                 #length_data = (code[(62-length_of_len):62][::-1])
    #                 #form = code[(62-length_of_len-2):(62-length_of_len)][::-1]
    #                 length_data = (aux[(62-length_of_len):62][::-1])
    #                 form = aux[(62-length_of_len-2):(62-length_of_len)][::-1]
    #                 lengthform = cipherfunctions.ecipherChaCha20(length_data + form, ChaCha20key, "hex")
    #                 length_data = int(lengthform[:-2],16)
    #                 form = lengthform[-2:]
    #                 #print("Lengthdadat", form, length_data,lengthform)
    #             else:
    #                 # length_data = int(code[(62-length_of_len):62][::-1],16)
    #                 # form = code[(62-length_of_len-2):(62-length_of_len)][::-1]
    #
    #                 length_data = int(aux[(62-length_of_len):62][::-1],16)
    #                 form = aux[(62-length_of_len-2):(62-length_of_len)][::-1]
    #         else:
    #             to_read = sizes[i] - 2
    #             #index_to_length = (62)
    #             index_to_length = (i*64 + 64) -2
    #             data = code[(index_to_length-to_read):index_to_length]
    #             rest = (length_of_len + 2) - to_read
    #
    # #TODO: case uin8 or very small argument
    # if rest != 0:
    #     print("TODO\n")
    #     exit(1)
    # if form[1]== "1":
    #     reajust = True
    # else:
    #     reajust = False
    # #fake_message = 'f' * len(hex(globalsettings.max_number_transactions)[2:]) * 2 + length_of_len * 'f' + 'f'*length_data
    # #This 2 is what we have to read to know how much the length occupies
    # fake_message = 2*'f'+ length_of_len * 'f' + 'f'*length_data
    # i = 0
    # #print(fake_message,form,length_data,reajust,dynamic,sizes)
    # if len(dynamic) == 0:
    #     #print("este",arguments, sizes)
    #     sizesn = dividemessage (arguments, fake_message, sizes, dynamic, reajust)
    # else:
    #     #Check the length of the dynamics if
    #     for i in dynamic:
    #         if arguments[i][-2:] == "[]":
    #             array_start = int(code[(i*64):(i*64 + 64)], 16) * 2
    #             lenelement = checklentotalmessage([arguments[i][:-2]])
    #             #how many...
    #             #print(lenelement)
    #             numberofelements = int(code[array_start:(array_start + 64)], 16)
    #             sizes[i] = numberofelements * lenelement[0][0]
    #         elif arguments[i] == "string" or arguments[i] == "bytes":
    #             array_start = int(code[(i*64):(i*64 + 64)], 16) * 2
    #             numberofelements = int(code[array_start:(array_start + 64)], 16) * 2
    #             c, r = divmod(numberofelements,64)
    #             if r != 0:
    #                 c += 1
    #             sizes[i] = c * 64
    #             #print(numberofelements,code[array_start:(array_start + 64)],divmod(numberofelements,64),sizes[i])
    #     #print("aquel", arguments, sizes)
    #     sizesn = dividemessage (arguments, fake_message, sizes, [], reajust)
    #     #print("sigo", sizesn)
    # #print(sizesn)
    # #Now I can read. Two cases, reajust and not
    # lengthsmin = 2 + length_of_len + len(form)
    # sizesnn = checkleninfirst(sizes,sizesn, lengthsmin)
    # #print(sizesnn)
    # sizes = copy.deepcopy(sizesnn)
    # if reajust:
    #     message = ""
    #     for i in range(len(sizesn)):
    #         if arguments[i] == "bool[]" or arguments[i] == "bool":
    #             continue
    #         if arguments[i][-2:] == "[]":
    #             array_start = int(code[(i*64):(i*64 + 64)], 16) * 2
    #             numberofelements = int(code[array_start:(array_start + 64)], 16)
    #             to_read = copy.deepcopy(sizes[i])
    #             #print(to_read,sizes)
    #             index_element= (array_start + 64 + 64)
    #             lenelement = checklentotalmessage([arguments[i][:-2]])
    #             for x in range(numberofelements):
    #                 if to_read >= lenelement[0][0]:
    #                     aux =  code[(index_element-lenelement[0][0]):index_element][::-1]
    #                     message += aux
    #                     index_element += 64
    #                     to_read -= lenelement[0][0]
    #                 else:
    #                     aux =  code[(index_element-to_read):index_element][::-1]
    #                     #print(aux)
    #                     message += aux
    #
    #         elif arguments[i] == "string" or arguments[i] == "bytes":
    #             array_start = int(code[(i*64):(i*64 + 64)], 16) * 2
    #             #print("entro2")
    #             #print(code[(i*64):(i*64 + 64)], code[array_start:(array_start + 64)], code[(array_start+64):(array_start+64+numberofelements)])
    #             #print("h",code[i*64:])
    #             #print("u",code)
    #             numberofelements = int(code[array_start:(array_start + 64)], 16) * 2
    #             aux = code[(array_start+64):(array_start+64+numberofelements)]
    #             #print(numberofelements, aux, code, array_start )
    #             #print(aux)
    #             message += aux
    #         else:
    #             if arguments[i][:5]== "bytes":
    #                 aux = code[(i*64):(i*64 + 64)]
    #                 aux= aux[:sizes[i]]
    #                 message += aux
    #             else:
    #                 aux = code[(i*64):(i*64 + 64)]
    #                 aux= aux[-sizes[i]:][::-1]
    #                 #print("aquí", code[(i*64):(i*64 + 64)], sizes[i], aux,len(aux))
    #                 message += aux
    # else:
    #     #print("por aqui")
    #     i = 0
    #     to_read_total = 2  + length_of_len + length_data
    #     #print(to_read_total)
    while i < len(sizes):
        if arguments[i] == "bool[]" or arguments[i] == "bool":
            pass
        elif arguments[i][-2:] == "[]":
            array_start = int(code[(i*64):(i*64 + 64)], 16) * 2
            # numberofelements = int(code[array_start:(array_start + 64)], 16)
            # to_read = copy.deepcopy(sizes[i])
            # index_element= (array_start + 64 + 64)
            # lenelement = checklentotalmessage([arguments[i][:-2]])
            # x = 0
            # while x < (numberofelements) and to_read_total != 0:
            #     if to_read_total >= lenelement[0][0]:
            #         aux =  code[(index_element-lenelement[0][0]):index_element][::-1]
            #         message += aux
            #         index_element += 64
            #         to_read_total -= lenelement[0][0]
            #     else:
            #         aux =  code[(index_element-to_read_total):index_element][::-1]
            #         message += aux
            #         to_read_total -= to_read_total

        elif arguments[i] == "string" or arguments[0] == "bytes":
            array_start = int(code[(i*64):(i*64 + 64)], 16) * 2
            # numberofelements = int(code[array_start:(array_start + 64)], 16) * 2
            # aux = code[(array_start+64):(array_start+64+numberofelements)]
            # message += aux
            # to_read_total -= numberofelements
        else:
            if arguments[i][:5]== "bytes":
                aux = code[(i*64):(i*64 + 64)]
                aux =  bytes.fromhex(aux)
                aux = ''.join('{:08b}'.format(x) for x in aux)
                # if to_read_total <= sizes[i] :
                #     aux = aux[:to_read_total]
                #     to_read_total -= to_read_total
                # else:
                #     aux= aux[:sizes[i]]
                #     to_read_total -= sizes[i]
                aux= aux[:sizes[i]]
                message += aux
            elif arguments[i] == "address":
                aux = code[(i*64):(i*64 + 64)]
                aux =  bytes.fromhex(aux)
                aux = ''.join('{:08b}'.format(x) for x in aux)
                aux= aux[-sizes[i]:]
                message += aux
                print("address", aux, message)
            elif arguments[i] == "uint256":
                print("messageuint256antes", message)
                aux = code[(i*64):(i*64 + 64)]
                aux = str(int(aux,16))
                length = len(aux)
                tmp = aux.rstrip('0')
                zeros = len(aux) - len(tmp)
                print(aux,length,zeros,tmp)
                values = dict_valuespossible[length][zeros]
                #print(values)
                # number_bits = bits_per_length[length][zeros]
                # print(number_bits)
                element = values.index(int(tmp))
                print(element)
                xtmp = '{0:08b}'.format(int(element))
                print(xtmp)
                print(sizes[i])
                if len(xtmp) < sizes[i]:
                    necessary = sizes[i] -len(xtmp)
                    xtmp = '0' * necessary + xtmp
                print(xtmp)
                message += xtmp
                print(message)
            else:
                aux = code[(i*64):(i*64 + 64)]
                #print("aux", aux)
                # if to_read_total <= sizes[i] :
                #     aux= aux[-to_read_total:][::-1]
                #     #print("aux", aux)
                #     to_read_total -= to_read_total
                # else:
                #     aux= aux[-sizes[i]:][::-1]
                #     #print("aux", aux)
                #     to_read_total -= sizes[i]
                #message += aux
        i+=1
    #print(message)
    #message = message[(2 + 2 + length_of_len):]
    #print(message)
    return message

#def createcall (arguments, message, sizesmn, dynamic, lengthsmin, dict_valuespossible, dict_field, zeros_field, list_sample,list_zeros, reajust = True, mantainsize = -1):
def createcall (arguments, message, sizesmn, dynamic, dict_valuespossible, dict_field, zeros_field, list_sample,list_zeros, reajust = True, mantainsize = -1):
    """
    Embeds the message in a function call
    """
    call = ""
    aux = ""
    auxmessage = copy.deepcopy(message)
    cont = ""
    dynamic_cont = 0
    fakemessage = ""
    dict_field = eval(dict_field)
    zeros_field = eval(zeros_field)
    if mantainsize > -1:
        fakemessage = 'f' * mantainsize
    for i in range(len(arguments)):
            #Speial case--> array
            #Check for the len and divide
        if  arguments[i] == "bool[]":
            #Will pick how many member of the array
            numberofelements = random.randint(1,3)
            numberelementinhex = hex(numberofelements)[2:]
            aux2 = ""
            numberelementinhex = hex(numberofelements)[2:]
            zeros = 64 - len(numberelementinhex)
            part1 = ("0" * zeros)
            aux2 += (part1 + numberelementinhex)
            #Now we pick random values for our booleans
            for el in range(numberofelements):
                aux2 += random.choice(["0000000000000000000000000000000000000000000000000000000000000001","0000000000000000000000000000000000000000000000000000000000000000"])
            position = int(len(cont)/2)
            position += (len(arguments)- i)* 32
            positionhex = hex(position)[2:]
            zeros = 64 - len(positionhex)
            part1 = ("0" * zeros)
            cont = cont + part1 + positionhex + aux2
            call = call + part1 + positionhex
            #print("c",call)
            aux += aux2
        elif arguments[i][-2:] == "[]":
            lenelement = checklentotalmessage([arguments[i][:-2]])
            #how many...
            #print(lenelement)
            numberofelements = math.ceil(int(sizesmn[i])/(lenelement)[0][0])
            #print("numero", numberofelements)
            # if reajust:
            #     if numberofelements < 2:
            #         numberofelements = 2
            sizesele = [lenelement[0][0]] * numberofelements
            #print(sizesele)
            argumentsnew = [arguments[i][:-2]] * numberofelements
            #print(argumentsnew)
            #TODO: Try with reajust = True
            sizesnew = dividemessage (argumentsnew, auxmessage[:sizesmn[i]], sizesele ,[], reajust= False)
            if i == 0:
                sizesnew = checkleninfirst(sizesele,sizesnew, lengthsmin)
            aux2 = ""
            numberelementinhex = hex(numberofelements)[2:]
            zeros = 64 - len(numberelementinhex)
            part1 = ("0" * zeros)
            aux2 += (part1 + numberelementinhex)
            #print(argumentsnew)
            for x in range(len(argumentsnew)):
                #print(x, aux2)
                auxm = auxmessage[:sizesnew[x]]
                if auxm == "":
                    #numberofbytes= random.randrange(1,8)
                    auxm = fakemessage[:sizesnew[x]]
                    #Try to make them all look similar
                    if auxm != "":
                        fillmessage = hex(random.getrandbits(4*len(fakemessage)))[2:]
                        fakemessage = fakemessage[sizesnew[x]:]
                        while int(fillmessage,16) == 0:
                            fillmessage = hex(random.getrandbits(4*len(fakemessage)))[2:]
                    else:
                        #Avoid 0s
                        numberofbytes= random.randrange(1,8)
                        fillmessage = hex(random.getrandbits(8*numberofbytes))[2:]
                        while int(fillmessage,16) == 0:
                            fillmessage = hex(random.getrandbits(8*numberofbytes))[2:]
                        if len(fillmessage) > sizesnew[x] and sizesnew[x] != 0:
                            fillmessage = fillmessage[:sizesnew[x]]
                        else:
                            fillmessage = hex(random.getrandbits(8))[2:]
                    #fillmessage = hex(random.getrandbits(8*numberofbytes))[2:]
                    auxm = copy.deepcopy(fillmessage)
                if arguments[i][:-2] == "address":
                    zeros = 64 - 40
                    randoms = 40 - len(auxm)
                    random.seed()
                    part1 = ("0" * zeros) + ''.join([hex(random.randint(0,15))[2:] for i in range(randoms)])
                else:
                    zeros = 64 - len(auxm)
                    #print(sizesnew,zeros)
                    part1 = ("0" * zeros)
                #print(part1)
                part2 = ""
                z = len(auxm) - 1
                while z >= 0:
                    part2 += auxm[z]
                    z -= 1
                #print(part2)
                auxmessage = auxmessage[sizesnew[x]:]
                aux2 += (part1 + part2)
                #print(aux2)
            position = int(len(cont)/2)
            position += (len(arguments)- i)* 32
            positionhex = hex(position)[2:]
            zeros = 64 - len(positionhex)
            part1 = ("0" * zeros)
            cont = cont + part1 + positionhex + aux2
            call = call + part1 + positionhex
            #print("c",call)
            aux += aux2
        elif arguments[i] == "string" or arguments[i] == "bytes":
            #Now bytes and strings.. padding at right
            #can be the lon we want so we have to view module to see remaining
            #use divmod
            #TODO: the len of the message cant be bigger than f * 64
            aux2 = ""
            part1 = auxmessage[:sizesmn[i]]
            if part1 == "":
                part1 = fakemessage[:sizesmn[i]]
                #Try to make them all look similar
                if part1 != "":
                    fillmessage = hex(random.getrandbits(4*len(fakemessage)))[2:]
                    fakemessage = fakemessage[sizesmn[i]:]
                    while int(fillmessage,16) == 0:
                        fillmessage = hex(random.getrandbits(4*len(fakemessage)))[2:]
                else:
                    #Avoid 0s
                    numberofbytes= random.randrange(1,8)
                    fillmessage = hex(random.getrandbits(8*numberofbytes))[2:]
                    while int(fillmessage,16) == 0:
                        fillmessage = hex(random.getrandbits(8*numberofbytes))[2:]
                # numberofbytes= random.randrange(1,8)
                # fillmessage = hex(random.getrandbits(8*numberofbytes))[2:]
                part1 = copy.deepcopy(fillmessage)
            #print(part1)
            leninhex = hex(int(len(part1)/2))[2:]
            zeros = 64 - len(leninhex)
            #print("zeros", zeros)
            part1 = ("0" * zeros)
            aux2 += (part1 + leninhex)
            #print("aux", aux2)
            c, r = divmod (sizesmn[i],64)
            if r != 0:
                zeros = 64 - r
            else:
                zeros = 0
            part1 = auxmessage[:sizesmn[i]]
            if part1 == "":
                c, r = divmod (len(fillmessage),64)
                if r != 0:
                    zeros = 64 - r
                else:
                    zeros = 0
                part1 = copy.deepcopy(fillmessage)
            auxmessage = auxmessage[sizesmn[i]:]
            part2 = ("0" * zeros)
            aux2 += (part1 + part2)
            if dynamic_cont == 0:
                position = int(len(call)/2 )
                position += (len(arguments)- i)* 32
                dynamic_cont = len(aux2)
            else:
                #print("call", call, len(call))
                #print("arguments", arguments, len(arguments) - i)
                #print("aux", aux)
                position = int(len(call)/2 )
                position +=  (len(arguments)- i)* 32 + int(len(aux)/2)
                dynamic_cont = position
            positionhex = hex(position)[2:]
            zeros = 64 - len(positionhex)
            part1 = ("0" * zeros)
            cont = cont + part1 + positionhex + aux2
            call = call + part1 + positionhex
            #print(call)
            aux += aux2

        else:
            if arguments[i][:5]== "bytes":
                part1 = auxmessage[:sizesmn[i]]
                part1 = hex(int(part1,2))[2:]
                if len(part1) % 2 !=0:
                    part1 = '0'+ part1
                if part1 == "":
                    part1 = fakemessage[:sizesmn[i]]
                    #Try to make them all look similar
                    if part1 != "":
                        fillmessage = hex(random.getrandbits(4*len(fakemessage)))[2:]
                        fakemessage = fakemessage[sizesmn[i]:]
                        while int(fillmessage,16) == 0:
                            fillmessage = hex(random.getrandbits(4*len(fakemessage)))[2:]
                    else:
                        #Avoid 0s
                        numberofbytes= random.randrange(1,8)
                        fillmessage = hex(random.getrandbits(8*numberofbytes))[2:]
                        while int(fillmessage,16) == 0:
                            fillmessage = hex(random.getrandbits(8*numberofbytes))[2:]
                        if len(fillmessage) > sizesmn[i] and sizesmn[i] != 0:
                            fillmessage = fillmessage[:sizesmn[i]]
                        else:
                            fillmessage = hex(random.getrandbits(8))[2:]
                    part1 = copy.deepcopy(fillmessage)
                auxmessage = auxmessage[sizesmn[i]:]
                zeros = 64 - len(part1)
                part2 = ("0" * zeros)
                call += (part1 + part2)
                cont += (part1 + part2)
            elif arguments[i] == "bool":
                #No message in here
                choice = random.choice(["0000000000000000000000000000000000000000000000000000000000000001","0000000000000000000000000000000000000000000000000000000000000000"])
                cont += choice
                call += choice
            #Adresses are always full
            elif arguments[i] == "address":
                part2 = auxmessage[:sizesmn[i]]
                part2 = hex(int(part2,2))[2:]
                if len(part2) % 2 !=0:
                    part2 = '0'+ part2
                zeros = 64 - 40
                randoms = 40 - len(part2)
                random.seed()
                part1 = ("0" * zeros) + ''.join([hex(random.randint(0,15))[2:] for i in range(randoms)])
                #auxm = auxmessage[:sizesmn[i]]
                # x = len(auxm) - 1
                # part2 = ""
                # while x >= 0:
                #     part2 += auxm[x]
                #     x -= 1
                auxmessage = auxmessage[sizesmn[i]:]
                # if mantainsize > -1:
                #     fakemessage = fakemessage[sizesmn[i]:]
                call += (part1 + part2)
                #print(call)
                cont += (part1 + part2)
            elif  arguments[i] == "uint256":
                if sys.version_info >= (3, 6):
                    length = random.choices(list(dict_field.keys()), list(dict_field.values()))[0]
                    tmp_dict = zeros_field[length]
                    zeros = random.choices(list(tmp_dict.keys()), list(tmp_dict.values()))[0]
                else:
                    length = random.choice(list_sample)
                    tmp_list = list_zeros[length]
                    zeros = random.choice(tmp_list)
                values = dict_valuespossible[length][zeros]
                part2 = auxmessage[:sizesmn[i]]
                part2 = hex(int(str(values[int(part2,2)]) + '0'*zeros))[2:]
                zeros = 64 - len(part2)
                part1 = ("0" * zeros)
                auxmessage = auxmessage[sizesmn[i]:]
                call += (part1 + part2)
                #print(call)
                cont += (part1 + part2)
            else:
                part2 = auxmessage[:sizesmn[i]]
                auxm = auxmessage[:sizesmn[i]]
                if auxm == "":
                    auxm = fakemessage[:sizesmn[i]]
                    #Try to make them all look similar
                    if auxm != "":
                        fillmessage = hex(random.getrandbits(4*len(fakemessage)))[2:]
                        fakemessage = fakemessage[sizesmn[i]:]
                        while int(fillmessage,16) == 0:
                            fillmessage = hex(random.getrandbits(4*len(fakemessage)))[2:]
                    else:
                        #Avoid 0s
                        numberofbytes= random.randrange(1,8)
                        fillmessage = hex(random.getrandbits(8*numberofbytes))[2:]
                        while int(fillmessage,16) == 0:
                            fillmessage = hex(random.getrandbits(8*numberofbytes))[2:]
                        if len(fillmessage) > sizesmn[i] and sizesmn[i] != 0:
                            fillmessage = fillmessage[:sizesmn[i]]
                        else:
                            fillmessage = hex(random.getrandbits(8))[2:]
                    auxm = copy.deepcopy(fillmessage)
                x = len(auxm) - 1
                zeros = 64 - len(auxm)
                part1 = ("0" * zeros)
                part2 = ""
                while x >= 0:
                    part2 += auxm[x]
                    x -= 1
                auxmessage = auxmessage[sizesmn[i]:]
                call += (part1 + part2)
                #print(call)
                cont += (part1 + part2)

    return call + aux


def createcall_old (arguments, message, sizesmn, dynamic, lengthsmin, reajust = True, mantainsize = -1):
    """
    Embeds the message in a function call
    """
    call = ""
    aux = ""
    auxmessage = copy.deepcopy(message)
    cont = ""
    dynamic_cont = 0
    fakemessage = ""
    if mantainsize > -1:
        fakemessage = 'f' * mantainsize
    for i in range(len(arguments)):
            #Speial case--> array
            #Check for the len and divide
        if  arguments[i] == "bool[]":
            #Will pick how many member of the array
            numberofelements = random.randint(1,3)
            numberelementinhex = hex(numberofelements)[2:]
            aux2 = ""
            numberelementinhex = hex(numberofelements)[2:]
            zeros = 64 - len(numberelementinhex)
            part1 = ("0" * zeros)
            aux2 += (part1 + numberelementinhex)
            #Now we pick random values for our booleans
            for el in range(numberofelements):
                aux2 += random.choice(["0000000000000000000000000000000000000000000000000000000000000001","0000000000000000000000000000000000000000000000000000000000000000"])
            position = int(len(cont)/2)
            position += (len(arguments)- i)* 32
            positionhex = hex(position)[2:]
            zeros = 64 - len(positionhex)
            part1 = ("0" * zeros)
            cont = cont + part1 + positionhex + aux2
            call = call + part1 + positionhex
            #print("c",call)
            aux += aux2
        elif arguments[i][-2:] == "[]":
            lenelement = checklentotalmessage([arguments[i][:-2]])
            #how many...
            #print(lenelement)
            numberofelements = math.ceil(int(sizesmn[i])/(lenelement)[0][0])
            #print("numero", numberofelements)
            # if reajust:
            #     if numberofelements < 2:
            #         numberofelements = 2
            sizesele = [lenelement[0][0]] * numberofelements
            #print(sizesele)
            argumentsnew = [arguments[i][:-2]] * numberofelements
            #print(argumentsnew)
            #TODO: Try with reajust = True
            sizesnew = dividemessage (argumentsnew, auxmessage[:sizesmn[i]], sizesele ,[], reajust= False)
            if i == 0:
                sizesnew = checkleninfirst(sizesele,sizesnew, lengthsmin)
            aux2 = ""
            numberelementinhex = hex(numberofelements)[2:]
            zeros = 64 - len(numberelementinhex)
            part1 = ("0" * zeros)
            aux2 += (part1 + numberelementinhex)
            #print(argumentsnew)
            for x in range(len(argumentsnew)):
                #print(x, aux2)
                auxm = auxmessage[:sizesnew[x]]
                if auxm == "":
                    #numberofbytes= random.randrange(1,8)
                    auxm = fakemessage[:sizesnew[x]]
                    #Try to make them all look similar
                    if auxm != "":
                        fillmessage = hex(random.getrandbits(4*len(fakemessage)))[2:]
                        fakemessage = fakemessage[sizesnew[x]:]
                        while int(fillmessage,16) == 0:
                            fillmessage = hex(random.getrandbits(4*len(fakemessage)))[2:]
                    else:
                        #Avoid 0s
                        numberofbytes= random.randrange(1,8)
                        fillmessage = hex(random.getrandbits(8*numberofbytes))[2:]
                        while int(fillmessage,16) == 0:
                            fillmessage = hex(random.getrandbits(8*numberofbytes))[2:]
                        if len(fillmessage) > sizesnew[x] and sizesnew[x] != 0:
                            fillmessage = fillmessage[:sizesnew[x]]
                        else:
                            fillmessage = hex(random.getrandbits(8))[2:]
                    #fillmessage = hex(random.getrandbits(8*numberofbytes))[2:]
                    auxm = copy.deepcopy(fillmessage)
                if arguments[i][:-2] == "address":
                    zeros = 64 - 40
                    randoms = 40 - len(auxm)
                    random.seed()
                    part1 = ("0" * zeros) + ''.join([hex(random.randint(0,15))[2:] for i in range(randoms)])
                else:
                    zeros = 64 - len(auxm)
                    #print(sizesnew,zeros)
                    part1 = ("0" * zeros)
                #print(part1)
                part2 = ""
                z = len(auxm) - 1
                while z >= 0:
                    part2 += auxm[z]
                    z -= 1
                #print(part2)
                auxmessage = auxmessage[sizesnew[x]:]
                aux2 += (part1 + part2)
                #print(aux2)
            position = int(len(cont)/2)
            position += (len(arguments)- i)* 32
            positionhex = hex(position)[2:]
            zeros = 64 - len(positionhex)
            part1 = ("0" * zeros)
            cont = cont + part1 + positionhex + aux2
            call = call + part1 + positionhex
            #print("c",call)
            aux += aux2
        elif arguments[i] == "string" or arguments[i] == "bytes":
            #Now bytes and strings.. padding at right
            #can be the lon we want so we have to view module to see remaining
            #use divmod
            #TODO: the len of the message cant be bigger than f * 64
            aux2 = ""
            part1 = auxmessage[:sizesmn[i]]
            if part1 == "":
                part1 = fakemessage[:sizesmn[i]]
                #Try to make them all look similar
                if part1 != "":
                    fillmessage = hex(random.getrandbits(4*len(fakemessage)))[2:]
                    fakemessage = fakemessage[sizesmn[i]:]
                    while int(fillmessage,16) == 0:
                        fillmessage = hex(random.getrandbits(4*len(fakemessage)))[2:]
                else:
                    #Avoid 0s
                    numberofbytes= random.randrange(1,8)
                    fillmessage = hex(random.getrandbits(8*numberofbytes))[2:]
                    while int(fillmessage,16) == 0:
                        fillmessage = hex(random.getrandbits(8*numberofbytes))[2:]
                # numberofbytes= random.randrange(1,8)
                # fillmessage = hex(random.getrandbits(8*numberofbytes))[2:]
                part1 = copy.deepcopy(fillmessage)
            #print(part1)
            leninhex = hex(int(len(part1)/2))[2:]
            zeros = 64 - len(leninhex)
            #print("zeros", zeros)
            part1 = ("0" * zeros)
            aux2 += (part1 + leninhex)
            #print("aux", aux2)
            c, r = divmod (sizesmn[i],64)
            if r != 0:
                zeros = 64 - r
            else:
                zeros = 0
            part1 = auxmessage[:sizesmn[i]]
            if part1 == "":
                c, r = divmod (len(fillmessage),64)
                if r != 0:
                    zeros = 64 - r
                else:
                    zeros = 0
                part1 = copy.deepcopy(fillmessage)
            auxmessage = auxmessage[sizesmn[i]:]
            part2 = ("0" * zeros)
            aux2 += (part1 + part2)
            if dynamic_cont == 0:
                position = int(len(call)/2 )
                position += (len(arguments)- i)* 32
                dynamic_cont = len(aux2)
            else:
                #print("call", call, len(call))
                #print("arguments", arguments, len(arguments) - i)
                #print("aux", aux)
                position = int(len(call)/2 )
                position +=  (len(arguments)- i)* 32 + int(len(aux)/2)
                dynamic_cont = position
            positionhex = hex(position)[2:]
            zeros = 64 - len(positionhex)
            part1 = ("0" * zeros)
            cont = cont + part1 + positionhex + aux2
            call = call + part1 + positionhex
            #print(call)
            aux += aux2

        else:
            if arguments[i][:5]== "bytes":
                part1 = auxmessage[:sizesmn[i]]
                if part1 == "":
                    part1 = fakemessage[:sizesmn[i]]
                    #Try to make them all look similar
                    if part1 != "":
                        fillmessage = hex(random.getrandbits(4*len(fakemessage)))[2:]
                        fakemessage = fakemessage[sizesmn[i]:]
                        while int(fillmessage,16) == 0:
                            fillmessage = hex(random.getrandbits(4*len(fakemessage)))[2:]
                    else:
                        #Avoid 0s
                        numberofbytes= random.randrange(1,8)
                        fillmessage = hex(random.getrandbits(8*numberofbytes))[2:]
                        while int(fillmessage,16) == 0:
                            fillmessage = hex(random.getrandbits(8*numberofbytes))[2:]
                        if len(fillmessage) > sizesmn[i] and sizesmn[i] != 0:
                            fillmessage = fillmessage[:sizesmn[i]]
                        else:
                            fillmessage = hex(random.getrandbits(8))[2:]
                    part1 = copy.deepcopy(fillmessage)
                auxmessage = auxmessage[sizesmn[i]:]
                zeros = 64 - len(part1)
                part2 = ("0" * zeros)
                call += (part1 + part2)
                cont += (part1 + part2)
            elif arguments[i] == "bool":
                #No message in here
                choice = random.choice(["0000000000000000000000000000000000000000000000000000000000000001","0000000000000000000000000000000000000000000000000000000000000000"])
                cont += choice
                call += choice
            #Adresses are always full
            elif arguments[i] == "address":
                part2 = auxmessage[:sizesmn[i]]
                zeros = 64 - 40
                randoms = 40 - len(part2)
                random.seed()
                part1 = ("0" * zeros) + ''.join([hex(random.randint(0,15))[2:] for i in range(randoms)])
                auxm = auxmessage[:sizesmn[i]]
                x = len(auxm) - 1
                part2 = ""
                while x >= 0:
                    part2 += auxm[x]
                    x -= 1
                auxmessage = auxmessage[sizesmn[i]:]
                if mantainsize > -1:
                    fakemessage = fakemessage[sizesmn[i]:]
                call += (part1 + part2)
                #print(call)
                cont += (part1 + part2)
            else:
                part2 = auxmessage[:sizesmn[i]]
                auxm = auxmessage[:sizesmn[i]]
                if auxm == "":
                    auxm = fakemessage[:sizesmn[i]]
                    #Try to make them all look similar
                    if auxm != "":
                        fillmessage = hex(random.getrandbits(4*len(fakemessage)))[2:]
                        fakemessage = fakemessage[sizesmn[i]:]
                        while int(fillmessage,16) == 0:
                            fillmessage = hex(random.getrandbits(4*len(fakemessage)))[2:]
                    else:
                        #Avoid 0s
                        numberofbytes= random.randrange(1,8)
                        fillmessage = hex(random.getrandbits(8*numberofbytes))[2:]
                        while int(fillmessage,16) == 0:
                            fillmessage = hex(random.getrandbits(8*numberofbytes))[2:]
                        if len(fillmessage) > sizesmn[i] and sizesmn[i] != 0:
                            fillmessage = fillmessage[:sizesmn[i]]
                        else:
                            fillmessage = hex(random.getrandbits(8))[2:]
                    auxm = copy.deepcopy(fillmessage)
                x = len(auxm) - 1
                zeros = 64 - len(auxm)
                part1 = ("0" * zeros)
                part2 = ""
                while x >= 0:
                    part2 += auxm[x]
                    x -= 1
                auxmessage = auxmessage[sizesmn[i]:]
                call += (part1 + part2)
                #print(call)
                cont += (part1 + part2)

    return call + aux



# b = 'a'*18
# size = dividemessage ([1,2,3], b , [4,10,6], [], reajust = True)
# print(size)
# size = dividemessage ([1,2,3], b , [4,10,6], [], reajust = False)
# print(size)
# size = dividemessage ([1,2,3,4,5], b , [4,6, 0, 10, 0], [2,4], reajust = True)
# print(size)
# size = dividemessage ([1,2,3,4,5], b , [4,6, 0, 10, 0], [2,4], reajust = False)
# print(size)
# b = 'a'*46
# size = dividemessage ([1,2,3,4,5], b , [4,6, 0, 10, 0], [2,4], reajust = True)
# print(size)
# size = dividemessage ([1,2,3,4,5], b , [4,6, 0, 10, 0], [2,4], reajust = False)
# print(size)
# call = createcall (["uint256", "uint32"], b,makeeven(["uint256", "uint32"], [23,23]), [], reajust = True)
# print(call)
# call = createcall (["uint256", "string"], b,makeeven(["uint256", "string"], [23,23]) , [1], reajust = True)
# print(call)
# print("adios")
# call = createcall (["uint256", "string"], b,makeeven(["uint256", "string"], [23,23]) , [1], reajust = False)
# print("adios")
# print(call)
# print("hola")
# call = createcall (["uint256", "string", "uint256[]"], b,makeeven(["uint256", "string", "uint256[]"], [15,16,15]) , [1,2], reajust = True)
# print("hola")
# print(call)
