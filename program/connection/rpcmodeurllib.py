import json
import urllib
from urllib.request import urlopen
from urllib import request, parse

__author__ = "Mar Gimenez Aguilar"
__version__ = "1"
__maintainer__ = "Mar Gimenez Aguilar"
__email__ = "mar.gimeneza@gmail.com"

rpcdir = 'https://rinkeby.infura.io/v3/5a2b0d460b2c420cae6352d6f1f2ac8f'
req = urllib.request.Request(rpcdir)
req.add_header('Content-Type', 'application/json; charset=utf-8')
body = {"jsonrpc":"2.0","method":"eth_getTransactionCount","params":["0x70e12500f7411d3ee3609ca55ebc552d5fbe4a1e", "pending"], "id":1}
jsondata = json.dumps(body)
jsondataasbytes = jsondata.encode('utf-8')   # needs to be bytes
print (jsondataasbytes)

with urlopen(req, jsondataasbytes) as r:
  result = json.loads(r.read().decode(r.headers.get_content_charset('utf-8')))

def rpcgetgasPrice(rpcdir,ID):
    """
    Returns the current gas price
    """
    #TODO check if localhost or 127....
    req = urllib.request.Request(rpcdir)
    req.add_header('Content-Type', 'application/json; charset=utf-8')
    body = {"jsonrpc":"2.0","method":"eth_gasPrice", "params":[]}
    body['id'] = ID
    jsondata = json.dumps(body)
    jsondataasbytes = jsondata.encode('utf-8')
    # call = '{"jsonrpc":"2.0","method":"eth_gasPrice","params":[],"id": '+ str(ID)+'}'
    # FNULL = open(os.devnull, 'wb')
    # result = subprocess.run(['curl',  '-H', app, '--data', call, rpcdir ], stdout=subprocess.PIPE, stderr=FNULL)
    with urlopen(req, jsondataasbytes) as r:
        result = json.loads(r.read().decode(r.headers.get_content_charset('utf-8')))
    return result['result'][2:]

def rpcgetBlockGasLimit(rpcdir,ID, block_number = ""):
    """
    Returns the gas limit for the block
    """
    req = urllib.request.Request(rpcdir)
    req.add_header('Content-Type', 'application/json; charset=utf-8')
    body = {"jsonrpc":"2.0","method":"eth_getBlockByNumber"}
    if block_number == "":
        body["params"]= ["pending",True]
    else:
        body["params"]= [block_number,True]
    body['id'] = ID
    jsondata = json.dumps(body)
    jsondataasbytes = jsondata.encode('utf-8')
    with urlopen(req, jsondataasbytes) as r:
        result = json.loads(r.read().decode(r.headers.get_content_charset('utf-8')))
    #print(result)
    return result['result']['gasLimit'][2:]

def rpcgetCode(contractAddress, rpcdir, ID, block_number = ""):
    req = urllib.request.Request(rpcdir)
    req.add_header('Content-Type', 'application/json; charset=utf-8')
    body = {"jsonrpc":"2.0","method":"eth_getCode"}
    body["params"]= [contractAddress,"latest"]
    body['id'] = ID
    #print(body)
    jsondata = json.dumps(body)
    jsondataasbytes = jsondata.encode('utf-8')
    with urlopen(req, jsondataasbytes) as r:
        result = json.loads(r.read().decode(r.headers.get_content_charset('utf-8')))
    #print(result)
    return result['result']


def rpcgetBlocktransactionsbyNumber (block_number, key_to_extract, key_value, key_to_retrieve, rpcdir,ID,tocontract=False):
    """
    Returns the valid transactions in a block that match a certain criteria or key
    """
    data_to_retrieve = []
    nonces = []
    req = urllib.request.Request(rpcdir)
    req.add_header('Content-Type', 'application/json; charset=utf-8')
    body = {"jsonrpc":"2.0","method":"eth_getBlockByNumber"}
    body["params"]= [block_number,True]
    body['id'] = ID
    jsondata = json.dumps(body)
    jsondataasbytes = jsondata.encode('utf-8')
    with urlopen(req, jsondataasbytes) as r:
        result = json.loads(r.read().decode(r.headers.get_content_charset('utf-8')))
    #print(result)
    block = result['result']
    #print(block)
    total_transactions = block['transactions']
    ID += 1
    for tx_hash in total_transactions:
        fullfill = True
        i = 0
        if tx_hash['to'] != None:
            if tocontract:
                if   rpcgetCode(tx_hash['to'], rpcdir, ID) == '0x':
                    fullfill = False
            else:
                if  rpcgetCode(tx_hash['to'], rpcdir, ID) != '0x':
                    fullfill = False
        while i < len(key_to_extract) and fullfill:
            if not(eval(str(eval("tx_hash[key_to_extract[i]]")) + str(eval("key_value[i]"))) or eval(str(eval("tx_hash[key_to_extract[i]].lower()")) +  eval("key_value[i].lower()"))):
                fullfill = False
            i += 1
        if fullfill:
            data_to_retrieve.append(tx_hash[key_to_retrieve])
            if type(data_to_retrieve[-1]) == str and data_to_retrieve[-1][0:2] == '0x':
                data_to_retrieve[-1] = data_to_retrieve[-1][2:]
            nonces.append(int(tx_hash['nonce'],16))
        ID += 1
    return data_to_retrieve, nonces

def rpcgetAccountNonce (senderAddress, rpcdir, ID):
    """
    Retrieve the current account nonce
    """
    tx_receipts = []
    req = urllib.request.Request(rpcdir)
    req.add_header('Content-Type', 'application/json; charset=utf-8')
    body = {"jsonrpc":"2.0","method":"eth_getTransactionCount"}
    body["params"]= [senderAddress,"pending"]
    body['id'] = ID
    #print(body)
    jsondata = json.dumps(body)
    jsondataasbytes = jsondata.encode('utf-8')
    with urlopen(req, jsondataasbytes) as r:
        result = json.loads(r.read().decode(r.headers.get_content_charset('utf-8')))
    # timeout_start = time.time()
    # while 'result' not in address and time.time() < (timeout_start + timeout):
    #     result = subprocess.run(['curl',  '-H', app, '--data', call, rpcdir ], stdout=subprocess.PIPE, stderr=FNULL)
    #     address = json.loads(result.stdout.decode('utf-8'))
    tx_receipts.append(result)
    ID += 1
    #print(tx_receipts[0])
    return tx_receipts[0]['result'][2:]

def rpcdeployContract(senderAddress, bytecode, rpcdir, ID, gas = '0x7A120',  times = 1):
    """
    Deploys a given contract into blockchain
    """
    req = urllib.request.Request(rpcdir)
    req.add_header('Content-Type', 'application/json; charset=utf-8')
    body = {"jsonrpc":"2.0","method":"eth_sendTransaction"}
    params = {}
    params['from'] = senderAddress
    params["gas"] = gas
    params["data"] = bytecode
    body['id'] = ID
    body['params'] = [params]
    print(body)
    jsondata = json.dumps(body)
    jsondataasbytes = jsondata.encode('utf-8')
    with urlopen(req, jsondataasbytes) as r:
        tx_hash = json.loads(r.read().decode(r.headers.get_content_charset('utf-8')))
    if not 'result' in tx_hash:
        print(tx_hash)
        exit(1)
    return tx_hash['result']

def rpcestimateGasdeployContract(senderAddress, bytecode, rpcdir, ID, gas = '0x7A120',  times = 1):
    """
    Estimates the necesary gas to deploy a given contract into the blockchain
    """
    req = urllib.request.Request(rpcdir)
    req.add_header('Content-Type', 'application/json; charset=utf-8')
    body = {"jsonrpc":"2.0","method":"eth_estimateGas"}
    params = {}
    params['from'] = senderAddress
    params["gas"] = gas
    params["data"] = bytecode
    body['id'] = ID
    body['params'] = [params]
    print(body)
    jsondata = json.dumps(body)
    jsondataasbytes = jsondata.encode('utf-8')
    with urlopen(req, jsondataasbytes) as r:
        tx_hash = json.loads(r.read().decode(r.headers.get_content_charset('utf-8')))
    if not 'result' in tx_hash:
        print(tx_hash)
        exit(1)
    return tx_hash['result']


def rpccheckContractDeployment (tx_hash, rpcdir, ID):
    """
    Check if a contract is deployed and return it address
    """
    req = urllib.request.Request(rpcdir)
    req.add_header('Content-Type', 'application/json; charset=utf-8')
    body = {"jsonrpc":"2.0","method":"eth_getTransactionCount"}
    body["params"]= [tx_hash]
    body['id'] = ID
    #print(body)
    jsondata = json.dumps(body)
    jsondataasbytes = jsondata.encode('utf-8')
    with urlopen(req, jsondataasbytes) as r:
        address = json.loads(r.read().decode(r.headers.get_content_charset('utf-8')))
    while 'result' not in address:
        address = json.loads(r.read().decode(r.headers.get_content_charset('utf-8')))
    address2= address['result']
    return address2['contractAddress']

def rpcestimateGasSendTransaction(senderAddress, receiverAddress,rpcdir,ID, gas = '0x7A120', gasPrice = -1, value = 0, data = "", nonce = -1 , times = 1):
    """
    Estimates the necessary gas to send a transaction
    """
    req = urllib.request.Request(rpcdir)
    req.add_header('Content-Type', 'application/json; charset=utf-8')
    body = {"jsonrpc":"2.0","method":"eth_estimateGas"}
    params = {}
    params['from'] = senderAddress
    params['to'] = receiverAddress
    params["data"] = data
    body['id'] = ID
    body['params'] = [params]
    print(body)
    jsondata = json.dumps(body)
    jsondataasbytes = jsondata.encode('utf-8')
    with urlopen(req, jsondataasbytes) as r:
        tx_hash = json.loads(r.read().decode(r.headers.get_content_charset('utf-8')))
    if not 'result' in tx_hash:
        print(tx_hash)
        exit(1)
    return tx_hash['result']

def rpcsendTransaction(senderAddress, receiverAddress,rpcdir,ID, gas = '0x7A120', gasPrice = -1, value = -1, data = "", nonce = -1 , times = 1):
    """
    Sends a transaction
    """
    req = urllib.request.Request(rpcdir)
    req.add_header('Content-Type', 'application/json; charset=utf-8')
    body = {"jsonrpc":"2.0","method":"eth_sendTransaction"}
    params = {}
    params['from'] = senderAddress
    params['to'] = receiverAddress
    params["data"] = data
    params['gas'] = gas
    body['id'] = ID
    if gasPrice != -1:
        params['gasPrice'] = gasPrice
    elif value != -1:
        params['value'] = value
    body['params'] = [params]
    print(body)
    jsondata = json.dumps(body)
    jsondataasbytes = jsondata.encode('utf-8')
    with urlopen(req, jsondataasbytes) as r:
        tx_hash = json.loads(r.read().decode(r.headers.get_content_charset('utf-8')))
    if not 'result' in tx_hash:
        print(tx_hash)
        exit(1)
    return tx_hash['result']

def rpccheckTransaction (tx_hash, rpcdir, ID):
    """
    Check if a given trasnaction is mined
    """
    address = ""
    app = "Content-Type: application/json"
    call = '{"jsonrpc":"2.0","method":"eth_getTransactionReceipt","params":["'+ tx_hash +'"],"id": '+ str(ID)+'}'
    FNULL = open(os.devnull, 'wb')
    result = subprocess.run(['curl',  '-H', app, '--data', call, rpcdir ], stdout=subprocess.PIPE, stderr=FNULL)
    address = json.loads(result.stdout.decode('utf-8'))
    while 'result' not in address:
        result = subprocess.run(['curl',  '-H', app, '--data', call, rpcdir ], stdout=subprocess.PIPE, stderr=FNULL)
        address = json.loads(result.stdout.decode('utf-8'))
    return True


ID = 1
result = rpcgetBlockGasLimit(rpcdir,ID,'0x4599ca')
print(int(result,16))
#tran = rpcgetBlocktransactionsbyNumber (hex(3656771), ['from'], ['== ' + str('0x70e12500f7411d3ee3609ca55ebc552d5fbe4a1e')], 'to', rpcdir,ID,tocontract=False)
print("tran", tran)
nonce = rpcgetAccountNonce ('0x70e12500f7411d3ee3609ca55ebc552d5fbe4a1e', rpcdir, ID)
print("nonce", nonce)
bytecode = '0x6060604052341561000f57600080fd5b61011c8061001e6000396000f3006060604052600436106049576000357c0100000000000000000000000000000000000000000000000000000000900463ffffffff168063165c4a1614604e5780638c12d8f014608b575b600080fd5b3415605857600080fd5b6075600480803590602001909190803590602001909190505060cf565b6040518082815260200191505060405180910390f35b3415609557600080fd5b60b2600480803590602001909190803590602001909190505060dc565b604051808381526020018281526020019250505060405180910390f35b6000818302905092915050565b6000808284019150828402905092509290505600a165627a7a72305820bd6b5df937fcf534e13a518b40f94d72fcaadf91fcbdf7bf7df1a81913e30e9a0029'
gas = hex(500000)
senderAddress = '0x70e12500f7411d3ee3609ca55ebc552d5fbe4a1e'
tx_hash = rpcestimateGasdeployContract(senderAddress, bytecode, rpcdir, ID, gas = gas)
print(tx_hash)
