#!/usr/bin/env python
"""
Contains all auxiliar function related with the connection
"""

__author__ = "Mar Gimenez Aguilar"
__version__ = "1"
__maintainer__ = "Mar Gimenez Aguilar"
__email__ = "mar.gimeneza@gmail.com"

def connectWeb3 ():
    """
    Return the web3 handler and option chosen
    """
    from . import w3mode
    print("Choose an option\n")
    print("1. IPC provider\n")
    print("2. HTTP provider\n")
    print("3. Default settings\n")
    Correct = False
    while not Correct:
        option = input()
        if option == "1":
            option = "ipc"
            Correct = True
        elif option == "2":
            option = "HTTP"
            Correct = True
        elif option == "3":
            option = "default"
            Correct = True
        else:
            print ("Option not valid\n")
    w3,w3option = w3mode.w3connect(option)
    return w3, w3option

def connectRPC():
    """
    Return the RPC handler or the Web3 one in case the user wants to connect
    using infura
    """
    from . import rpcmode
    print("Choose an option\n")
    print("1. HTTP provider\n")
    print("2. Default\n")
    Correct = False
    while not Correct:
        option = input()
        if option == "1":
            print("Introduce url\n")
            rpc = input()
            w3 = ""
            Correct = True
            if "infura" in rpc:
                try:
                    from . import w3mode
                    w3 = w3mode.w3simpleconnection(rpc)
                except ImportError:
                    print("For connection with infura, web3py must be installed\n")
                    Correct = False
        elif option == "2":
            rpc = 'localhost:8545'
            Correct = True
            w3 = ""
        else:
            print ("Option not valid\n")
    return rpc, w3
