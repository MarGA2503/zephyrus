

----------------------------------------------------
Zephyrus: Ethereum Steganographic Tool
----------------------------------------------------

This a Python tool that allows embedding and retrieving a message using the Ethereum cryptocurrency blockchain. It allows the user to choose among different methods and options regarding the way of introducing this information.


----------------------------------------------------
Prerequisites
----------------------------------------------------

-Python 3.5 or higher.
-PyCryptodome

Zephyrus can connect to the Ethereum blockchain via geth (go-ethereum client) or using an infura node. To connect with geth via ipc or infura, this library is also necesary:

-Web3py


----------------------------------------------------
Installing
----------------------------------------------------

Please, for the necessary libaries, refer to the respective pages:

-PyCryptodome: https://pycryptodome.readthedocs.io/en/latest/src/installation.html#
-Web3py: https://web3py.readthedocs.io/en/stable/quickstart.html


----------------------------------------------------
Usage
----------------------------------------------------


Infura
----------------------------------------------------
To be able to use infura, first an infura account must be created.

Then, a project must be created too. After that, a selected endpoint (Ethereum net) must be selected and a link similar to this would be generated:

https://NETWORK.infura.io/YOUR_API_KEY

This would we the url passed to Zephyrus, using the http option.

Note that, to be able to use this option, the user must also has the private key of the ethereum account.

----------------------------------------------------
Geth
----------------------------------------------------
To be able to connect geth with Zephyrus, the ipc or rpc directory must be passed to the program. Zephyrus is able to recognise geth's default values. If the rpc option want to be used, it should be habilitated and the sender account unlocked beforehand:

geth --rpc --rpcapi db,eth,net,web3,personal --unlock <YOUR_ACCOUNT_ADDRESS> console

The user will have three attemps to introduce the password and then, the address will be unlock until geth is stopped.

----------------------------------------------------
Zephyrus
----------------------------------------------------
To use Zephyrus, simply write in the command line:

python3 Zephyrus.py

And follow the instructions. If the results of the tests are to be replicated, the program can be executed with an extra argument, indicating how many times should send the message using the same configuration. It will, also, print the mean of the measure of the time it took to execute. For example, the following instruction, would execute the program 20 times:

python3 Zephyrus.py 20

The inputs the user will have to introduce may vary depending of the chosen method. Some things are common:

- All addresses and hashes must start with 0x
- The sender address is always passed directly to the program
- The receiver/s address are spected to be in a text file, as well as the contract address and transaction hashes, when needed. Be careful with extra spaces, tabulations and line breaks
- The contracts files must be either .bin or .abi
- The password asked when using geth is the password used in geth, not the private key
- The message extracted is saved in the current directory as "message". Windows users may have to add the extension to the file
- Transaction hashes are also saved in the current directory. They can also be retrieved using a block explorer like Etherscan

The default version of the program allows the user to use it a bit "faster", by choosing some values by itself. However, they are not the stealthiest ones, so personalization is recommended.

----------------------------------------------------
Notes and comments
----------------------------------------------------
As, this version is still a proof-of-concept, the maximun amount of transaction per message is 255, so if a larger message is introduced, the program will ask for a shorter one and end.
The user is the responsable of choosing the contracts and the functions to use in each case. Some functions may not be accesible to the user, because of the contract configuration. 

