#!/usr/bin/env python
"""
Contains the program main function
"""


import os
import sys
from getpass import getpass
import time
import copy
from program.auxiliar import readbytes
from program.auxiliar import maindialoges
from program.auxiliar import cipherfunctions
from program.auxiliar import messagefunctions
from program.modules import receiveraddressmodule
from program.modules import swarmhashmodule
from program.modules import bytecodemodule
from program.modules import constructormodule
from program.modules import functioncallmodule
from program.modules import gaslimitmodule
from program.modules import gaspricemodule
from program.modules import valuemodule

__author__ = "Mar Gimenez Aguilar"
__credits__ = ["Mar Gimenez Aguilar", "Jose M de Fuentes Garcia-Romero de Tejada",
"Lorena Gonzalez Manzano"]
__version__ = "1"
__maintainer__ = "Mar Gimenez Aguilar"
__email__ = "mar.gimeneza@gmail.com"


default = False
cipher = True
password = ""
AESkey = ""
ChaCha20key = ""


def extractmessage(ntimes, timeV):
    """
    Main function to extract the message.
    """
    global password
    global AESkey
    global ChaCha20key
    default =  maindialoges.choosedefault()
    method, type_hex, type_bin = maindialoges.choosemethodextract()
    AESkey, nonce = maindialoges.choosecipher(default)
    if not default:
        #password = maindialoges.chooseorder()
        password=""
    if AESkey != "":
        AESkey = cipherfunctions.strechKey(AESkey,nonce="")
        ChaCha20key = cipherfunctions.generateChaCha20key(AESkey)
    #print(method)
    if timeV:
        if method != "contractcodemodule.extractContractCodeMethod(connection, ChaCha20key, password, default)":
            print("Please introduce the path to the hash folder\n")
            Correct = False
            while  not Correct:
                tx = input()
                if os.path.lexists(tx):
                    Correct = True
                else:
                    print("Path not valid\n")
            tx_hashes, address = readbytes.readThash(tx)
    else:
        if method != "contractcodemodule.extractContractCodeMethod(connection, ChaCha20key, password, default)":
            print("Please introduce the path to the hash\n")
            Correct = False
            while  not Correct:
                tx = input()
                if os.path.lexists(tx):
                    Correct = True
                else:
                    print("Path not valid\n")

            tx_hashes, address = readbytes.readThash(tx)
    connection =  maindialoges.choosegethconnectionmethod(default)
    message = eval(method)
    if password != "":
        message = messagefunctions.ordermessage([message],password)
        message = message[0]
    medium_cipher = 0
    if timeV:
        ciphering_execution = []
        for i in range(ntimes):
            message_time = copy.deepcopy(message)
            start_time_perf = time.perf_counter()
            if AESkey != "":
                if type_hex:
                    message_time = cipherfunctions.ecipherAES(message_time, AESkey, nonce, "hex")
                else:
                    message_time = cipherfunctions.ecipherAES(message_time, AESkey, nonce, "hex")
                time_ciphering_perf = time.perf_counter() - start_time_perf
                medium_cipher += time_ciphering_perf
                ciphering_execution.append(time_ciphering_perf)
        time_ciphering_perf = medium_cipher/ntimes
    if AESkey != "":
        if type_hex:
            message = cipherfunctions.ecipherAES(message, AESkey, nonce, "hex")
        else:
            message = cipherfunctions.ecipherAES(message, AESkey, nonce, "hex")
    if timeV and AESkey != "":
        print("Deciphering time (average): {} seconds ".format(time_ciphering_perf))
        print("Deciphering time per execution: {} seconds ".format(ciphering_execution))
    readbytes.savemessage(".", message)

def insertmessage(ntimes, timeV):
    """
    Main function to insert the message.
    """
    global password
    global AESkey
    global ChaCha20key
    default =  maindialoges.choosedefault()
    method, type_hex, type_bin = maindialoges.choosemethodinsert()
    message_file, file_format =  maindialoges.insertmessagefile()
    AESkey, nonce = maindialoges.choosecipher(default)
    medium_cipher = 0
    data, TextVBin = readbytes.readbytes(message_file)
        #TODO: Windows users please habilitate this option
        # if type_hex:
        #     data = messagefunctions.insertextensiondata(data, file_format)
        # else:
        #     data = messagefunctions.insertextensiondata(data, file_format)
        #     TextVBin = messagefunctions.insertextensionbin(TextVBin, file_format)
    if AESkey != "":
        AESkey = cipherfunctions.strechKey(AESkey,nonce="")
        ChaCha20key = cipherfunctions.generateChaCha20key(AESkey)
        if timeV:
            ciphering_execution = []
            for i in range(ntimes):
                data_time = copy.deepcopy(data)
                TextVBin_time = ''
                start_time_perf = time.perf_counter()
                if type_hex:
                    data_time =  cipherfunctions.cipherAES(data_time, AESkey, nonce, "hex")
                else:
                    data_time =  bytes.fromhex(cipherfunctions.cipherAES(data_time, AESkey, nonce, "hex"))
                    TextVBin_time = ''.join('{:08b}'.format(x) for x in data_time)
                    data_time = data_time.hex()
                time_ciphering_perf = time.perf_counter() - start_time_perf
                medium_cipher += time_ciphering_perf
                ciphering_execution.append(time_ciphering_perf)
            time_ciphering_perf = medium_cipher/ntimes
        if type_hex:
            data =  cipherfunctions.cipherAES(data, AESkey, nonce, "hex")
        else:
            data =  bytes.fromhex(cipherfunctions.cipherAES(data, AESkey, nonce, "hex"))
            TextVBin = ''.join('{:08b}'.format(x) for x in data)
            data = data.hex()
    if not default:
        #password =maindialoges.chooseorder()
        password=""
    if password != "":
        if type_hex:
            data =  messagefunctions.disordermessage([data], password)
            data = data[0]
        else:
            data =  messagefunctions.disordermessage([data], password)
            data = bytes.fromhex(data[0])
            TextVBin = ''.join('{:08b}'.format(x) for x in data)
    #print(method)
    connection =  maindialoges.choosegethconnectionmethod(default)
    tx_hashes = eval(method)
    #if method != "ContractCodeMethod(TextVBin,connection)":
    if timeV and AESkey != "":
        print("Ciphering time(average): {} seconds".format(time_ciphering_perf))
        print("Ciphering time per execution: {} seconds".format(ciphering_execution))
    if timeV:
        if not os.path.exists("transactions"):
            os.mkdir("transactions")
        #print(timeV)
        readbytes.writeThash("./transactions", tx_hashes, times=ntimes)
    else:
        readbytes.writeThash(".", tx_hashes)


if __name__ == '__main__':
    """
    Main function. Check the command line arguments. If none, the program executes
    only once. If one, the program executes that number and presents time
    measurements
    """
    if len(sys.argv) == 1:
        ntimes = 1
        timeV = False
    elif len(sys.argv) == 2:
        ntimes = int(sys.argv[1])
        timeV = True
    else:
        print("Too much arguments\n")
        exit(1)
    print("Choose an option\n")
    print("1.Insert message\n")
    print("2.Extract message\n")
    Correct = False
    while not Correct:
        a = input()
        if a == "1":
            Correct = True
            insertmessage(ntimes,timeV)
        elif a == "2":
            Correct = True
            extractmessage(ntimes,timeV)
        else:
            print("Option not valid\n")
